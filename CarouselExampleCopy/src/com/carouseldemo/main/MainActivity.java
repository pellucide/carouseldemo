package com.carouseldemo.main;

import com.carouseldemo.controls.Carousel;
import com.carouseldemo.controls.Carousel.ImageAdapter;
import com.carouseldemo.controls.Carousel.OnItemClickListener;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity {
   	int drawble_ids[] = {
   			R.drawable.cat,
   			R.drawable.hippo,
   			R.drawable.monkey,
   			R.drawable.mouse,
   			R.drawable.panda,
   			R.drawable.rabbit,
   	};
   	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Carousel carousel = (Carousel)findViewById(R.id.carousel);
        
		//carousel.SetImages(drawble_ids);
        carousel.setOnItemClickListener(new OnItemClickListener(){

			public void onItemClick(ImageAdapter parent, View view,
					int position, long id) {				
				Toast.makeText(MainActivity.this, "Position=" + position, Toast.LENGTH_SHORT).show();				
				//view.setPressed(true);
			}
        	
        });
    }
    
}

