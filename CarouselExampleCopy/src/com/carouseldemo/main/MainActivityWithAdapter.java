package com.carouseldemo.main;

import com.carouseldemo.controls.CarouselAdapter;
import com.carouseldemo.controls.CarouselAdapter.SelectionChangedListener;
import com.carouseldemo.controls.CarouselWithAdapter;
import com.carouseldemo.controls.CarouselWithAdapter.OnItemClickListener;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivityWithAdapter extends Activity implements SelectionChangedListener{
	private String[] mDemoItems;		
	boolean[] selectedPositions;
    LinearLayout selectedList;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main1);
        CarouselWithAdapter carousel = (CarouselWithAdapter)findViewById(R.id.carousel);
        selectedList = (LinearLayout)findViewById(R.id.selectedList);
        
        carousel.setOnItemClickListener(new OnItemClickListener(){
			public void onItemClick(CarouselAdapter parent, View view,
					int position, long id) {				
				Toast.makeText(MainActivityWithAdapter.this, "Position=" + position, Toast.LENGTH_SHORT).show();				
				//view.setPressed(true);
			}
        });
		mDemoItems = getResources().getStringArray(R.array.demoitems);
		selectedPositions = new boolean[mDemoItems.length];
		carousel.getAdapter().addListener(this);
    }

	@Override
	public void onSelectedListChange(boolean[] selectedPositions) {
		LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		selectedList.removeAllViews();
		for(int i = 0; i< selectedPositions.length; i++) {
			if (selectedPositions[i]) {
				RelativeLayout itemView = (RelativeLayout) inflator.inflate(R.layout.selected_list_item, null);
				((TextView)itemView.findViewById(R.id.cancelButton)).setText(mDemoItems[i]);
				selectedList.addView(itemView);
			}
		}

	}
}

