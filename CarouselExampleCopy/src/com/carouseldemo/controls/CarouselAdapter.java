package com.carouseldemo.controls;

import java.util.ArrayList;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuffXfermode;
import android.graphics.Bitmap.Config;
import android.graphics.PorterDuff.Mode;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.carouseldemo.main.R;

public class CarouselAdapter extends BaseAdapter {
	private Context mContext;
	private CarouselView[] mCarouselViews;		
	private String[] mDemoItems;		
	private boolean[] selectedList;
	public interface SelectionChangedListener {
		public void onSelectedListChange(boolean[] selectedPositions) ;
	}
	ArrayList<SelectionChangedListener> listeners = new ArrayList<SelectionChangedListener>();
	
	public void addListener(SelectionChangedListener listener) {
		listeners.add(listener);
	}

	public void removeListener(SelectionChangedListener listener) {
		listeners.remove(listener);
	}

	public CarouselAdapter(Context c) {
		mContext = c;
		mDemoItems = c.getResources().getStringArray(R.array.demoitems);
	}		
					
	public void SetImages(TypedArray array){
		SetImages(array, true);
	}
	
	public void SetImages(TypedArray array, boolean reflected){
		Drawable[] drawables = new Drawable[array.length()];
		for(int i = 0; i< array.length(); i++) {
			drawables[i] = array.getDrawable(i);
		}
	    SetImages(drawables, reflected);
	}
	
	
	public void SetImages(int[] drawable_ids) {
		Drawable[] drawables = new Drawable[drawable_ids.length];
		for(int i = 0; i< drawable_ids.length; i++) {
			drawables[i] = mContext.getResources().getDrawable(drawable_ids[i]);
		}
		
	    SetImages(drawables, false);
	}

	public void SetImages(Drawable drawables[], boolean reflected){
		final int reflectionGap = 4;
		mCarouselViews = new CarouselView[drawables.length];
		
		LayoutInflater inflator = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		selectedList = new boolean[drawables.length];
		for(int i = 0; i< drawables.length; i++) {
			if(reflected) {
				Bitmap originalImage = ((BitmapDrawable)drawables[i]).getBitmap();
			    drawables[i] = new BitmapDrawable(getBitmapWithReflection(originalImage, reflectionGap)); 
			}
			
			CarouselView carouselView = (CarouselView) inflator.inflate(R.layout.carousel_element, null);
			((ImageView)carouselView.findViewById(R.id.imageView1)).setImageDrawable(drawables[i]);
            //((TextView)carouselView.findViewById(R.id.cancelButton)).setText(";;"+i+" Item;;");
            ((TextView)carouselView.findViewById(R.id.cancelButton)).setText(mDemoItems[i]+"("+i+")");
			carouselView.setIndex(i);
			mCarouselViews[i] = carouselView;
			CheckBox cb = (CheckBox)carouselView.findViewById(R.id.checkBox1);
			cb.setTag(i);
			cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView,
						boolean isChecked) {
					int position = (Integer) buttonView.getTag();
					Toast.makeText(mContext, "Position=" + position+",isChecked="+isChecked, Toast.LENGTH_SHORT).show();				
					selectedList[position] = isChecked;
					for(SelectionChangedListener l: listeners) {
						l.onSelectedListChange(selectedList);
					}
				}
			});
		}
	}
	public boolean[] getSelectedList() {
		return selectedList;
	}

	Bitmap getBitmapWithReflection(Bitmap origBitmap, int reflectionGap) {
			int width = origBitmap.getWidth();
			int height = origBitmap.getHeight();

			// This will not scale but will flip on the Y axis
			Matrix matrix = new Matrix();
			matrix.preScale(1, -1);
			
			// Create a Bitmap with the flip matrix applied to it.
			// We only want the bottom half of the image
			Bitmap reflectionImage = Bitmap.createBitmap(origBitmap, 0,
					height / 2, width, height / 2, matrix, false);

			// Create a new bitmap with same width but taller to fit
			// reflection
			Bitmap bitmapWithReflection = Bitmap.createBitmap(width,
					(height + height / 2), Config.ARGB_8888);

			// Create a new Canvas with the bitmap that's big enough for
			// the image plus gap plus reflection
			Canvas canvas = new Canvas(bitmapWithReflection);
			// Draw in the original image
			canvas.drawBitmap(origBitmap, 0, 0, null);
			// Draw in the gap
			Paint deafaultPaint = new Paint();
			canvas.drawRect(0, height, width, height + reflectionGap,
					deafaultPaint);
			// Draw in the reflection
			canvas.drawBitmap(reflectionImage, 0, height + reflectionGap,
					null);

			// Create a shader that is a linear gradient that covers the
			// reflection
			Paint paint = new Paint();
			LinearGradient shader = new LinearGradient(0,
					origBitmap.getHeight(), 0,
					bitmapWithReflection.getHeight() + reflectionGap,
					0x70ffffff, 0x00ffffff, TileMode.CLAMP);
			// Set the paint to use this shader (linear gradient)
			paint.setShader(shader);
			// Set the Transfer mode to be porter duff and destination in
			paint.setXfermode(new PorterDuffXfermode(Mode.DST_IN));
			// Draw a rectangle using the paint with our linear gradient
			canvas.drawRect(0, height, width,
					bitmapWithReflection.getHeight() + reflectionGap, paint);
			
			return bitmapWithReflection;
	}

	public int getCount() {
		if(mCarouselViews == null)
			return 0;
		else
			return mCarouselViews.length;
	}

	public Object getItem(int position) {
		return position;
	}

	public long getItemId(int position) {
		return position;
	}

	public CarouselView getView(int position, View convertView, ViewGroup parent) {
		return mCarouselViews[position];
	}

}