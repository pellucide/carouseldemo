package com.carouseldemo.controls;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import com.carouseldemo.main.R;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.ContextMenu;
import android.view.GestureDetector;
import android.view.HapticFeedbackConstants;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SoundEffectConstants;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.accessibility.AccessibilityEvent;
import android.view.animation.Transformation;
import android.widget.AbsSpinner;
import android.widget.ListView;

public class CarouselWithAdapter extends ViewGroup implements GestureDetector.OnGestureListener {
    /**
     * The position of the first child displayed
     */
    @ViewDebug.ExportedProperty
    int mFirstPosition = 0;
    
    Calendar mCal;

    /**
     * The offset in pixels from the top of the CarouselAdapter to the top
     * of the view to select during the next layout.
     */
    int mSpecificTop;

    /**
     * Position from which to start looking for mSyncRowId
     */
    int mSyncPosition;

    /**
     * Row id to look for when data has changed
     */
    long mSyncRowId = INVALID_ROW_ID;

    /**
     * Height of the view when mSyncPosition and mSyncRowId where set
     */
    long mSyncHeight;

    /**
     * True if we need to sync to mSyncRowId
     */
    boolean mNeedSync = false;

    /**
     * Indicates whether to sync based on the salection or position. Possible
     * values are {@link #SYNC_SELECTED_POSITION} or
     * {@link #SYNC_FIRST_POSITION}.
     */
    int mSyncMode;

    /**
     * Our height after the last layout
     */
    private int mLayoutHeight;

    /**
     * Sync based on the selected child
     */
    static final int SYNC_SELECTED_POSITION = 0;

    /**
     * Sync based on the first child displayed
     */
    static final int SYNC_FIRST_POSITION = 1;

    /**
     * Maximum amount of time to spend in {@link #findSyncPosition()}
     */
    static final int SYNC_MAX_DURATION_MILLIS = 100;

    /**
     * Indicates that this view is currently being laid out.
     */
    boolean mInLayout = false;

    /**
     * The listener that receives notifications when an item is selected.
     */
    OnItemSelectedListener mOnItemSelectedListener;

    /**
     * The listener that receives notifications when an item is clicked.
     */
    OnItemClickListener mOnItemClickListener;

    /**
     * The listener that receives notifications when an item is long clicked.
     */
    OnItemLongClickListener mOnItemLongClickListener;

    /**
     * True if the data has changed since the last layout
     */
    boolean mDataChanged;

    /**
     * The position within the adapter's data set of the item to select
     * during the next layout.
     */
    @ViewDebug.ExportedProperty    
    int mNextSelectedPosition = INVALID_POSITION;

    /**
     * The item id of the item to select during the next layout.
     */
    long mNextSelectedRowId = INVALID_ROW_ID;

    /**
     * The position within the adapter's data set of the currently selected item.
     */
    @ViewDebug.ExportedProperty    
    int mSelectedPosition = INVALID_POSITION;

    /**
     * The item id of the currently selected item.
     */
    long mSelectedRowId = INVALID_ROW_ID;

    /**
     * View to show if there are no items to show.
     */
    private View mEmptyView;
    
    /**
     * the number of items to show.
     */
    private int mItemsToShowCount;

    /**
     * The number of items in the current adapter.
     */
    @ViewDebug.ExportedProperty
    int mItemCount;

    /**
     * The number of items in the adapter before a data changed event occured.
     */
    int mOldItemCount;

    /**
     * Represents an invalid position. All valid positions are in the range 0 to 1 less than the
     * number of items in the current adapter.
     */
    public static final int INVALID_POSITION = -1;

    /**
     * Represents an empty or invalid row id
     */
    public static final long INVALID_ROW_ID = Long.MIN_VALUE;

    /**
     * The last selected position we used when notifying
     */
    int mOldSelectedPosition = INVALID_POSITION;
    
    /**
     * The id of the last selected position we used when notifying
     */
    long mOldSelectedRowId = INVALID_ROW_ID;

    /**
     * Indicates what focusable state is requested when calling setFocusable().
     * In addition to this, this view has other criteria for actually
     * determining the focusable state (such as whether its empty or the text
     * filter is shown).
     *
     * @see #setFocusable(boolean)
     * @see #checkFocus()
     */
    private boolean mDesiredFocusableState;
    private boolean mDesiredFocusableInTouchModeState;

    private SelectionNotifier mSelectionNotifier;
    /**
     * When set to true, calls to requestLayout() will not propagate up the parent hierarchy.
     * This is used to layout the children during a layout pass.
     */
    boolean mBlockLayoutRequests = false;


    /**
     * Interface definition for a callback to be invoked when an item in this
     * CarouselAdapter has been clicked.
     */
    public interface OnItemClickListener {

        /**
         * Callback method to be invoked when an item in this CarouselAdapter has
         * been clicked.
         * <p>
         * Implementers can call getItemAtPosition(position) if they need
         * to access the data associated with the selected item.
         *
         * @param parent The CarouselAdapter where the click happened.
         * @param view The view within the CarouselAdapter that was clicked (this
         *            will be a view provided by the adapter)
         * @param position The position of the view in the adapter.
         * @param id The row id of the item that was clicked.
         */
        void onItemClick(CarouselAdapter parent, View view, int position, long id);
    }

    /**
     * Register a callback to be invoked when an item in this CarouselAdapter has
     * been clicked.
     *
     * @param listener The callback that will be invoked.
     */
    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    /**
     * @return The callback to be invoked with an item in this CarouselAdapter has
     *         been clicked, or null id no callback has been set.
     */
    public final OnItemClickListener getOnItemClickListener() {
        return mOnItemClickListener;
    }

    /**
     * Call the OnItemClickListener, if it is defined.
     *
     * @param view The view within the CarouselAdapter that was clicked.
     * @param position The position of the view in the adapter.
     * @param id The row id of the item that was clicked.
     * @return True if there was an assigned OnItemClickListener that was
     *         called, false otherwise is returned.
     */
    public boolean performItemClick(View view, int position, long id) {
        if (mOnItemClickListener != null) {
            //playSoundEffect(SoundEffectConstants.CLICK);
            mOnItemClickListener.onItemClick(getAdapter(), view, position, id);
            return true;
        }

        return false;
    }

    /**
     * Interface definition for a callback to be invoked when an item in this
     * view has been clicked and held.
     */
    public interface OnItemLongClickListener {
        /**
         * Callback method to be invoked when an item in this view has been
         * clicked and held.
         *
         * Implementers can call getItemAtPosition(position) if they need to access
         * the data associated with the selected item.
         *
         * @param parent The AbsListView where the click happened
         * @param view The view within the AbsListView that was clicked
         * @param position The position of the view in the list
         * @param id The row id of the item that was clicked
         *
         * @return true if the callback consumed the long click, false otherwise
         */
        boolean onItemLongClick(CarouselAdapter parent, View view, int position, long id);
    }


    /**
     * Register a callback to be invoked when an item in this CarouselAdapter has
     * been clicked and held
     *
     * @param listener The callback that will run
     */
    public void setOnItemLongClickListener(OnItemLongClickListener listener) {
        if (!isLongClickable()) {
            setLongClickable(true);
        }
        mOnItemLongClickListener = listener;
    }

    /**
     * @return The callback to be invoked with an item in this CarouselAdapter has
     *         been clicked and held, or null id no callback as been set.
     */
    public final OnItemLongClickListener getOnItemLongClickListener() {
        return mOnItemLongClickListener;
    }

    /**
     * Interface definition for a callback to be invoked when
     * an item in this view has been selected.
     */
    public interface OnItemSelectedListener {
        /**
         * Callback method to be invoked when an item in this view has been
         * selected.
         *
         * Impelmenters can call getItemAtPosition(position) if they need to access the
         * data associated with the selected item.
         *
         * @param parent The CarouselAdapter where the selection happened
         * @param view The view within the CarouselAdapter that was clicked
         * @param position The position of the view in the adapter
         * @param id The row id of the item that is selected
         */
        void onItemSelected(CarouselAdapter parent, View view, int position, long id);

        /**
         * Callback method to be invoked when the selection disappears from this
         * view. The selection can disappear for instance when touch is activated
         * or when the adapter becomes empty.
         *
         * @param parent The CarouselAdapter that now contains no selected item.
         */
        void onNothingSelected(CarouselAdapter parent);
    }


    /**
     * Register a callback to be invoked when an item in this CarouselAdapter has
     * been selected.
     *
     * @param listener The callback that will run
     */
    public void setOnItemSelectedListener(OnItemSelectedListener listener) {
        mOnItemSelectedListener = listener;
    }

    public final OnItemSelectedListener getOnItemSelectedListener() {
        return mOnItemSelectedListener;
    }

    /**
     * Extra menu information provided to the
     * {@link android.view.View.OnCreateContextMenuListener#onCreateContextMenu(ContextMenu, View, ContextMenuInfo) }
     * callback when a context menu is brought up for this CarouselAdapter.
     *
     */
    public static class AdapterContextMenuInfo implements ContextMenu.ContextMenuInfo {
        public AdapterContextMenuInfo(View targetView, int position, long id) {
            this.targetView = targetView;
            this.position = position;
            this.id = id;
        }

        /**
         * The child view for which the context menu is being displayed. This
         * will be one of the children of this CarouselAdapter.
         */
        public View targetView;

        /**
         * The position in the adapter for which the context menu is being
         * displayed.
         */
        public int position;

        /**
         * The row id of the item for which the context menu is being displayed.
         */
        public long id;
    }


    /**
     * This method is not supported and throws an UnsupportedOperationException when called.
     *
     * @param child Ignored.
     *
     * @throws UnsupportedOperationException Every time this method is invoked.
     */
    @Override
    public void addView(View child) {
        throw new UnsupportedOperationException("addView(View) is not supported in CarouselAdapter");
    }

    /**
     * This method is not supported and throws an UnsupportedOperationException when called.
     *
     * @param child Ignored.
     * @param index Ignored.
     *
     * @throws UnsupportedOperationException Every time this method is invoked.
     */
    @Override
    public void addView(View child, int index) {
        throw new UnsupportedOperationException("addView(View, int) is not supported in CarouselAdapter");
    }

    /**
     * This method is not supported and throws an UnsupportedOperationException when called.
     *
     * @param child Ignored.
     * @param params Ignored.
     *
     * @throws UnsupportedOperationException Every time this method is invoked.
     */
    @Override
    public void addView(View child, LayoutParams params) {
        throw new UnsupportedOperationException("addView(View, LayoutParams) "
                + "is not supported in CarouselAdapter");
    }

    /**
     * This method is not supported and throws an UnsupportedOperationException when called.
     *
     * @param child Ignored.
     * @param index Ignored.
     * @param params Ignored.
     *
     * @throws UnsupportedOperationException Every time this method is invoked.
     */
    @Override
    public void addView(View child, int index, LayoutParams params) {
        throw new UnsupportedOperationException("addView(View, int, LayoutParams) "
                + "is not supported in CarouselAdapter");
    }

    /**
     * This method is not supported and throws an UnsupportedOperationException when called.
     *
     * @param child Ignored.
     *
     * @throws UnsupportedOperationException Every time this method is invoked.
     */
    @Override
    public void removeView(View child) {
        throw new UnsupportedOperationException("removeView(View) is not supported in CarouselAdapter");
    }

    /**
     * This method is not supported and throws an UnsupportedOperationException when called.
     *
     * @param index Ignored.
     *
     * @throws UnsupportedOperationException Every time this method is invoked.
     */
    @Override
    public void removeViewAt(int index) {
        throw new UnsupportedOperationException("removeViewAt(int) is not supported in CarouselAdapter");
    }

    /**
     * This method is not supported and throws an UnsupportedOperationException when called.
     *
     * @throws UnsupportedOperationException Every time this method is invoked.
     */
    @Override
    public void removeAllViews() {
        throw new UnsupportedOperationException("removeAllViews() is not supported in CarouselAdapter");
    }


    /**
     * Return the position of the currently selected item within the adapter's data set
     *
     * @return int Position (starting at 0), or {@link #INVALID_POSITION} if there is nothing selected.
     */
    @ViewDebug.CapturedViewProperty
    public int getSelectedItemPosition() {
        return mNextSelectedPosition;
    }

    /**
     * @return The id corresponding to the currently selected item, or {@link #INVALID_ROW_ID}
     * if nothing is selected.
     */
    @ViewDebug.CapturedViewProperty
    public long getSelectedItemId() {
        return mNextSelectedRowId;
    }


    /**
     * @return The data corresponding to the currently selected item, or
     * null if there is nothing selected.
     */
    public Object getSelectedItem() {
        CarouselAdapter adapter = getAdapter();
        int selection = getSelectedItemPosition();
        if (adapter != null && adapter.getCount() > 0 && selection >= 0) {
            return adapter.getItem(selection);
        } else {
            return null;
        }
    }

    /**
     * Get the position within the adapter's data set for the view, where view is a an adapter item
     * or a descendant of an adapter item.
     *
     * @param view an adapter item, or a descendant of an adapter item. This must be visible in this
     *        CarouselAdapter at the time of the call.
     * @return the position within the adapter's data set of the view, or {@link #INVALID_POSITION}
     *         if the view does not correspond to a list item (or it is not currently visible).
     */
    public int getPositionForView(View view) {
        View listItem = view;
        try {
            View v;
            while (!(v = (View) listItem.getParent()).equals(this)) {
                listItem = v;
            }
        } catch (ClassCastException e) {
            // We made it up to the window without find this list view
            return INVALID_POSITION;
        }

        // Search the children for the list item
        final int childCount = getChildCount();
        for (int i = 0; i < childCount; i++) {
            if (getChildAt(i).equals(listItem)) {
                return mFirstPosition + i;
            }
        }

        // Child not found!
        return INVALID_POSITION;
    }

    /**
     * Returns the position within the adapter's data set for the first item
     * displayed on screen.
     *
     * @return The position within the adapter's data set
     */
    public int getFirstVisiblePosition() {
        return mFirstPosition;
    }

    /**
     * Returns the position within the adapter's data set for the last item
     * displayed on screen.
     *
     * @return The position within the adapter's data set
     */
    public int getLastVisiblePosition() {
        return mFirstPosition + getChildCount() - 1;
    }


    /**
     * Sets the view to show if the adapter is empty
     */
    public void setEmptyView(View emptyView) {
        mEmptyView = emptyView;

        final CarouselAdapter adapter = getAdapter();
        final boolean empty = ((adapter == null) || adapter.isEmpty());
        updateEmptyStatus(empty);
    }

    /**
     * When the current adapter is empty, the CarouselAdapter can display a special view
     * call the empty view. The empty view is used to provide feedback to the user
     * that no data is available in this CarouselAdapter.
     *
     * @return The view to show if the adapter is empty.
     */
    public View getEmptyView() {
        return mEmptyView;
    }

    /**
     * Indicates whether this view is in filter mode. Filter mode can for instance
     * be enabled by a user when typing on the keyboard.
     *
     * @return True if the view is in filter mode, false otherwise.
     */
    boolean isInFilterMode() {
        return false;
    }

    @Override
    public void setFocusable(boolean focusable) {
        final CarouselAdapter adapter = getAdapter();
        final boolean empty = adapter == null || adapter.getCount() == 0;

        mDesiredFocusableState = focusable;
        if (!focusable) {
            mDesiredFocusableInTouchModeState = false;
        }

        super.setFocusable(focusable && (!empty || isInFilterMode()));
    }

    @Override
    public void setFocusableInTouchMode(boolean focusable) {
        final CarouselAdapter adapter = getAdapter();
        final boolean empty = adapter == null || adapter.getCount() == 0;

        mDesiredFocusableInTouchModeState = focusable;
        if (focusable) {
            mDesiredFocusableState = true;
        }

        super.setFocusableInTouchMode(focusable && (!empty || isInFilterMode()));
    }

    void checkFocus() {
        final CarouselAdapter adapter = getAdapter();
        final boolean empty = adapter == null || adapter.getCount() == 0;
        final boolean focusable = !empty || isInFilterMode();
        // The order in which we set focusable in touch mode/focusable may matter
        // for the client, see View.setFocusableInTouchMode() comments for more
        // details
        super.setFocusableInTouchMode(focusable && mDesiredFocusableInTouchModeState);
        super.setFocusable(focusable && mDesiredFocusableState);
        if (mEmptyView != null) {
            updateEmptyStatus((adapter == null) || adapter.isEmpty());
        }
    }

    /**
     * Update the status of the list based on the empty parameter.  If empty is true and
     * we have an empty view, display it.  In all the other cases, make sure that the listview
     * is VISIBLE and that the empty view is GONE (if it's not null).
     */
    private void updateEmptyStatus(boolean empty) {
        if (isInFilterMode()) {
            empty = false;
        }

        if (empty) {
            if (mEmptyView != null) {
                mEmptyView.setVisibility(View.VISIBLE);
                setVisibility(View.GONE);
            } else {
                // If the caller just removed our empty view, make sure the list view is visible
                setVisibility(View.VISIBLE);
            }

            // We are now GONE, so pending layouts will not be dispatched.
            // Force one here to make sure that the state of the list matches
            // the state of the adapter.
            if (mDataChanged) {           
                layout(getLeft(), getTop(), getRight(), getBottom()); 
                //this.onLayout(false,getLeft(), getTop(), getRight(), getBottom()); 
            }
        } else {
            if (mEmptyView != null) mEmptyView.setVisibility(View.GONE);
            setVisibility(View.VISIBLE);
        }
    }

    /**
     * Gets the data associated with the specified position in the list.
     *
     * @param position Which data to get
     * @return The data associated with the specified position in the list
     */
    public Object getItemAtPosition(int position) {
        CarouselAdapter adapter = getAdapter();
        return (adapter == null || position < 0) ? null : adapter.getItem(position);
    }

    public long getItemIdAtPosition(int position) {
        CarouselAdapter adapter = getAdapter();
        return (adapter == null || position < 0) ? INVALID_ROW_ID : adapter.getItemId(position);
    }

    @Override
    public void setOnClickListener(OnClickListener l) {
        throw new RuntimeException("Don't call setOnClickListener for an CarouselAdapter. "
                + "You probably want setOnItemClickListener instead");
    }

    /**
     * Override to prevent freezing of any views created by the adapter.
     */
    @Override
    protected void dispatchSaveInstanceState(SparseArray<Parcelable> container) {
        dispatchFreezeSelfOnly(container);
    }

    /**
     * Override to prevent thawing of any views created by the adapter.
     */
    @Override
    protected void dispatchRestoreInstanceState(SparseArray<Parcelable> container) {
        dispatchThawSelfOnly(container);
    }

    class AdapterDataSetObserver extends DataSetObserver {
        private Parcelable mInstanceState = null;
        @Override
        public void onChanged() {
            mDataChanged = true;
            mOldItemCount = mItemCount;
            mItemCount = getAdapter().getCount();

            // Detect the case where a cursor that was previously invalidated has
            // been repopulated with new data.
            if (getAdapter().hasStableIds() && mInstanceState != null
                    && mOldItemCount == 0 && mItemCount > 0) {
                onRestoreInstanceState(mInstanceState);
                mInstanceState = null;
            } else {
                rememberSyncState();
            }
            checkFocus();
            requestLayout();
        }

        @Override
        public void onInvalidated() {
            mDataChanged = true;

            if (getAdapter().hasStableIds()) {
                // Remember the current state for the case where our hosting activity is being
                // stopped and later restarted
                mInstanceState = onSaveInstanceState();
            }

            // Data is invalid so we should reset our state
            mOldItemCount = mItemCount;
            mItemCount = 0;
            mSelectedPosition = INVALID_POSITION;
            mSelectedRowId = INVALID_ROW_ID;
            mNextSelectedPosition = INVALID_POSITION;
            mNextSelectedRowId = INVALID_ROW_ID;
            mNeedSync = false;
            checkSelectionChanged();

            checkFocus();
            requestLayout();
        }

        public void clearSavedState() {
            mInstanceState = null;
        }
    }

    private class SelectionNotifier extends Handler implements Runnable {
        public void run() {
            if (mDataChanged) {
                // Data has changed between when this SelectionNotifier
                // was posted and now. We need to wait until the CarouselAdapter
                // has been synched to the new data.
                post(this);
            } else {
                fireOnSelected();
            }
        }
    }

    void selectionChanged() {
        if (!mSuppressSelectionChanged) {
            selectionChanged1();
        }
    }

    void selectionChanged1() {
        if (mOnItemSelectedListener != null) {
            if (mInLayout || mBlockLayoutRequests) {
                // If we are in a layout traversal, defer notification
                // by posting. This ensures that the view tree is
                // in a consistent state and is able to accomodate
                // new layout or invalidate requests.
                if (mSelectionNotifier == null) {
                    mSelectionNotifier = new SelectionNotifier();
                }
                mSelectionNotifier.post(mSelectionNotifier);
            } else {
                fireOnSelected();
            }
        }

        // we fire selection events here not in View
        if (mSelectedPosition != ListView.INVALID_POSITION && isShown() && !isInTouchMode()) {
            sendAccessibilityEvent(AccessibilityEvent.TYPE_VIEW_SELECTED);
        }
    }
    
    private void fireOnSelected() {
        if (mOnItemSelectedListener == null)
            return;

        int selection = this.getSelectedItemPosition();
        if (selection >= 0) {
            View v = getSelectedView();
            mOnItemSelectedListener.onItemSelected(getAdapter(), v, selection,
                    getAdapter().getItemId(selection));
        } else {
            mOnItemSelectedListener.onNothingSelected(getAdapter());
        }
    }

    @Override
    protected boolean canAnimate() {
        return super.canAnimate() && getAdapter().getCount() > 0;
    }

    void handleDataChanged() {
        final int count = mItemCount;
        boolean found = false;

        if (count > 0) {

            int newPos;

            // Find the row we are supposed to sync to
            if (mNeedSync) {
                // Update this first, since setNextSelectedPositionInt inspects
                // it
                mNeedSync = false;

                // See if we can find a position in the new data with the same
                // id as the old selection
                newPos = findSyncPosition();
                if (newPos >= 0) {
                    // Verify that new selection is selectable
                    int selectablePos = lookForSelectablePosition(newPos, true);
                    if (selectablePos == newPos) {
                        // Same row id is selected
                        setNextSelectedPositionInt(newPos);
                        found = true;
                    }
                }
            }
            if (!found) {
                // Try to use the same position if we can't find matching data
                newPos = getSelectedItemPosition();

                // Pin position to the available range
                if (newPos >= count) {
                    newPos = count - 1;
                }
                if (newPos < 0) {
                    newPos = 0;
                }

                // Make sure we select something selectable -- first look down
                int selectablePos = lookForSelectablePosition(newPos, true);
                if (selectablePos < 0) {
                    // Looking down didn't work -- try looking up
                    selectablePos = lookForSelectablePosition(newPos, false);
                }
                if (selectablePos >= 0) {
                    setNextSelectedPositionInt(selectablePos);
                    checkSelectionChanged();
                    found = true;
                }
            }
        }
        if (!found) {
            // Nothing is selected
            mSelectedPosition = INVALID_POSITION;
            mSelectedRowId = INVALID_ROW_ID;
            mNextSelectedPosition = INVALID_POSITION;
            mNextSelectedRowId = INVALID_ROW_ID;
            mNeedSync = false;
            checkSelectionChanged();
        }
    }

    void checkSelectionChanged() {
        if ((mSelectedPosition != mOldSelectedPosition) || (mSelectedRowId != mOldSelectedRowId)) {
            selectionChanged1();
            mOldSelectedPosition = mSelectedPosition;
            mOldSelectedRowId = mSelectedRowId;
        }
    }

    /**
     * Searches the adapter for a position matching mSyncRowId. The search starts at mSyncPosition
     * and then alternates between moving up and moving down until 1) we find the right position, or
     * 2) we run out of time, or 3) we have looked at every position
     *
     * @return Position of the row that matches mSyncRowId, or {@link #INVALID_POSITION} if it can't
     *         be found
     */
    int findSyncPosition() {
        int count = mItemCount;

        if (count == 0) {
            return INVALID_POSITION;
        }

        long idToMatch = mSyncRowId;
        int seed = mSyncPosition;

        // If there isn't a selection don't hunt for it
        if (idToMatch == INVALID_ROW_ID) {
            return INVALID_POSITION;
        }

        // Pin seed to reasonable values
        seed = Math.max(0, seed);
        seed = Math.min(count - 1, seed);

        long endTime = SystemClock.uptimeMillis() + SYNC_MAX_DURATION_MILLIS;

        long rowId;

        // first position scanned so far
        int first = seed;

        // last position scanned so far
        int last = seed;

        // True if we should move down on the next iteration
        boolean next = false;

        // True when we have looked at the first item in the data
        boolean hitFirst;

        // True when we have looked at the last item in the data
        boolean hitLast;

        // Get the item ID locally (instead of getItemIdAtPosition), so
        // we need the adapter
        CarouselAdapter adapter = getAdapter();
        if (adapter == null) {
            return INVALID_POSITION;
        }

        while (SystemClock.uptimeMillis() <= endTime) {
            rowId = adapter.getItemId(seed);
            if (rowId == idToMatch) {
                // Found it!
                return seed;
            }

            hitLast = last == count - 1;
            hitFirst = first == 0;

            if (hitLast && hitFirst) {
                // Looked at everything
                break;
            }

            if (hitFirst || (next && !hitLast)) {
                // Either we hit the top, or we are trying to move down
                last++;
                seed = last;
                // Try going up next time
                next = false;
            } else if (hitLast || (!next && !hitFirst)) {
                // Either we hit the bottom, or we are trying to move up
                first--;
                seed = first;
                // Try going down next time
                next = true;
            }

        }

        return INVALID_POSITION;
    }

    /**
     * Find a position that can be selected (i.e., is not a separator).
     *
     * @param position The starting position to look at.
     * @param lookDown Whether to look down for other positions.
     * @return The next selectable position starting at position and then searching either up or
     *         down. Returns {@link #INVALID_POSITION} if nothing can be found.
     */
    int lookForSelectablePosition(int position, boolean lookDown) {
        return position;
    }

    	
    void setSelectedPositionInt1(int position) {
        mSelectedPosition = position;
        mSelectedRowId = getItemIdAtPosition(position);
    }
    	
    /**
     * Utility to keep mSelectedPosition and mSelectedRowId in sync
     * @param position Our current position
     */
    void setSelectedPositionInt(int position) {
    	setSelectedPositionInt1(position);
        // Updates any metadata we keep about the selected item.
        updateSelectedItemMetadata();
    }

    /**
     * Utility to keep mNextSelectedPosition and mNextSelectedRowId in sync
     * @param position Intended value for mSelectedPosition the next time we go
     * through layout
     */
    void setNextSelectedPositionInt(int position) {
        mNextSelectedPosition = position;
        mNextSelectedRowId = getItemIdAtPosition(position);
        // If we are trying to sync to the selection, update that too
        if (mNeedSync && mSyncMode == SYNC_SELECTED_POSITION && position >= 0) {
            mSyncPosition = position;
            mSyncRowId = mNextSelectedRowId;
        }
    }

    
    /**
     * Handles left, right, and clicking
     * @see android.view.View#onKeyDown
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            
        case KeyEvent.KEYCODE_DPAD_LEFT:
            ////if (movePrevious()) {
                playSoundEffect(SoundEffectConstants.NAVIGATION_LEFT);
            ////}
            return true;

        case KeyEvent.KEYCODE_DPAD_RIGHT:
            /////if (moveNext()) {
                playSoundEffect(SoundEffectConstants.NAVIGATION_RIGHT);
            ////}
            return true;

        case KeyEvent.KEYCODE_DPAD_CENTER:
        case KeyEvent.KEYCODE_ENTER:
            mReceivedInvokeKeyDown = true;
            // fallthrough to default handling
        }
        
        return super.onKeyDown(keyCode, event);
    }
    
    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
        case KeyEvent.KEYCODE_DPAD_CENTER:
        case KeyEvent.KEYCODE_ENTER: {
            
            if (mReceivedInvokeKeyDown) {
                if (mItemCount > 0) {
    
                    dispatchPress(mSelectedChild);
                    postDelayed(new Runnable() {
                        public void run() {
                            dispatchUnpress();
                        }
                    }, ViewConfiguration.getPressedStateDuration());
    
                    int selectedIndex = mSelectedPosition - mFirstPosition;
                    performItemClick(getChildAt(selectedIndex), mSelectedPosition, mAdapter
                            .getItemId(mSelectedPosition));
                }
            }
            
            // Clear the flag
            mReceivedInvokeKeyDown = false;
            
            return true;
        }
        }

        return super.onKeyUp(keyCode, event);
    }    
        
    @Override
    protected void onFocusChanged(boolean gainFocus, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
        
        /*
         * The gallery shows focus by focusing the selected item. So, give
         * focus to our selected item instead. We steal keys from our
         * selected item elsewhere.
         */
        if (gainFocus && mSelectedChild != null) {
            mSelectedChild.requestFocus(direction);
        }

    }       
    
    /**
     * Remember enough information to restore the screen state when the data has
     * changed.
     *
     */
    void rememberSyncState() {
        if (getChildCount() > 0) {
            mNeedSync = true;
            mSyncHeight = mLayoutHeight;
            if (mSelectedPosition >= 0) {
                // Sync the selection state
                View v = getChildAt(mSelectedPosition - mFirstPosition);
                mSyncRowId = mNextSelectedRowId;
                mSyncPosition = mNextSelectedPosition;
                if (v != null) {
                    mSpecificTop = v.getTop();
                }
                mSyncMode = SYNC_SELECTED_POSITION;
            } else {
                // Sync the based on the offset of the first view
                View v = getChildAt(0);
                CarouselAdapter adapter = getAdapter();
                if (mFirstPosition >= 0 && mFirstPosition < adapter.getCount()) {
                    mSyncRowId = adapter.getItemId(mFirstPosition);
                } else {
                    mSyncRowId = NO_ID;
                }
                mSyncPosition = mFirstPosition;
                if (v != null) {
                    mSpecificTop = v.getTop();
                }
                mSyncMode = SYNC_FIRST_POSITION;
            }
        }
    }
    
    class RecycleBin {
        private final SparseArray<View> mScrapHeap = new SparseArray<View>();

        public void put(int position, View v) {
            mScrapHeap.put(position, v);
        }
        
        View get(int position) {
            // System.out.print("Looking for " + position);
            View result = mScrapHeap.get(position);
            if (result != null) {
                // System.out.println(" HIT");
                mScrapHeap.delete(position);
            } else {
                // System.out.println(" MISS");
            }
            return result;
        }

        void clear() {
            final SparseArray<View> scrapHeap = mScrapHeap;
            final int count = scrapHeap.size();
            for (int i = 0; i < count; i++) {
                final View view = scrapHeap.valueAt(i);
                if (view != null) {
                    removeDetachedView(view, true);
                }
            }
            scrapHeap.clear();
        }
    }	
    
    
    int mHeightMeasureSpec;
    int mWidthMeasureSpec;
    CarouselAdapter mAdapter;
    public CarouselAdapter getAdapter() {
    	return mAdapter;
    }

    public View getSelectedView() {
    	if (mItemCount > 0 && mSelectedPosition >= 0) {
    		return getChildAt(mSelectedPosition - mFirstPosition);
    	} else {
    		return null;
    	}
    }

    public void setSelection(int position) {
    	setSelectionInt(position, false);
    }

    /**
     * Jump directly to a specific item in the adapter data.
     */
    public void setSelection(int position, boolean animate) {
    	// Animate only if requested position is already on screen somewhere
    	boolean shouldAnimate = animate && mFirstPosition <= position &&
    			position <= mFirstPosition + getChildCount() - 1;
    	setSelectionInt(position, shouldAnimate);
    }
    /**
     * Makes the item at the supplied position selected.
     * 
     * @param position Position to select
     * @param animate Should the transition be animated
     * 
     */
    void setSelectionInt(int position, boolean animate) {
    	if (position != mOldSelectedPosition) {
    		mBlockLayoutRequests = true;
    		int delta  = position - mSelectedPosition;
    		setNextSelectedPositionInt(position);
    		layoutMe(delta, animate);
    		mBlockLayoutRequests = false;
    	}
    }

    int mSelectionLeftPadding = 0;
    int mSelectionTopPadding = 0;
    int mSelectionRightPadding = 0;
    int mSelectionBottomPadding = 0;

    final RecycleBin mRecycler = new RecycleBin();
    private DataSetObserver mDataSetObserver;
	
	// [start] Static private members

	/**
	 * Tag for a class logging
	 */
	private static final String TAG = CarouselWithAdapter.class.getSimpleName();

	/**
	 * If logging should be inside class
	 */
	private static final boolean localLOGV = false;
	
	/**
	 * Default min quantity of images
	 */
	private static final int MIN_QUANTITY = 3;
	
	/**
	 * Default max quantity of images
	 */
	private static final int MAX_QUANTITY = 12;
	
	/**
	 * Max theta
	 */
	private static final float MAX_THETA = 15.0f;

	/**
     * Duration in milliseconds from the start of a scroll during which we're
     * unsure whether the user is scrolling or flinging.
     */
    private static final int SCROLL_TO_FLING_UNCERTAINTY_TIMEOUT = 250;
	
	// [end]
	
	// [start] Private members		
    
    /**
     * The info for adapter context menu
     */
    private AdapterContextMenuInfo mContextMenuInfo;
    
    /**
     * How long the transition animation should run when a child view changes
     * position, measured in milliseconds.
     */
    private final int defaultAnimationDuration = 300;
    private int mAnimationDuration = defaultAnimationDuration;    
    
	/**
	 * Camera to make 3D rotation
	 */
	private Camera mCamera = new Camera();

    /**
     * Sets mSuppressSelectionChanged = false. This is used to set it to false
     * in the future. It will also trigger a selection changed.
     */
    private Runnable mDisableSuppressSelectionChangedRunnable = new Runnable() {
        public void run() {
            mSuppressSelectionChanged = false;
            selectionChanged();
        }
    };
    	
    /**
     * The position of the item that received the user's down touch.
     */
    private int mDownTouchPosition;
	
    /**
     * The view of the item that received the user's down touch.
     */
    private View mDownTouchView;
        
    /**
     * Executes the delta rotations from a fling or scroll movement. 
     */
    private FlingRotateRunnable mFlingRunnable = new FlingRotateRunnable();
    
    /**
     * Helper for detecting touch gestures.
     */
    private GestureDetector mGestureDetector;
	
    /**
     * Gravity for the widget
     */
    private CarouselAffinity mAffinity;
    	
    /**
     * If true, this onScroll is the first for this user's drag (remember, a
     * drag sends many onScrolls).
     */
    private boolean mIsFirstScroll;
    
    /**
     * Set max qantity of images
     */
    private int mMaxQuantity = MAX_QUANTITY;
    
    /**
     * Set min quantity of images
     */
    private int mMinQuantity = MIN_QUANTITY;
    
    /**
     * If true, we have received the "invoke" (center or enter buttons) key
     * down. This is checked before we action on the "invoke" key up, and is
     * subsequently cleared.
     */
    private boolean mReceivedInvokeKeyDown;
        
    /**
     * The currently selected item's child.
     */
    private View mSelectedChild;
        

    /**
     * Whether to continuously callback on the item selected listener during a
     * fling.
     */
    private boolean mShouldCallbackDuringFling = true;
    
    /**
     * Whether to callback when an item that is not selected is clicked.
     */
    private boolean mShouldCallbackOnUnselectedItemClick = true;
    
    /**
     * When fling runnable runs, it resets this to false. Any method along the
     * path until the end of its run() can set this to true to abort any
     * remaining fling. For example, if we've reached either the leftmost or
     * rightmost item, we will set this to true.
     */
    private boolean mShouldStopFling;
        
    /**
     * If true, do not callback to item selected listener. 
     */
    private boolean mSuppressSelectionChanged;
    
	/**
	 * The axe angle
	 */
    //private float mTheta = (float)(15.0f*(Math.PI/180.0));	
    private float mTheta = (float)(-9.0f*(Math.PI/180.0));	
    
    /**
     * If items should be reflected
     */
    private boolean mUseReflection;
    
    private float mAngleSpan;
    private float mStartAngle;
    private float mEndAngle;

    // [end]
    
    // [start] Constructors

	public CarouselWithAdapter(Context context)  {
		this(context, null);
	}
	
	public CarouselWithAdapter(Context context, AttributeSet attrs)  {
		this(context, attrs, 0);
	}
	
	enum CarouselAffinity {LEFT, RIGHT, BOTTOM, TOP };

	
	private Point mCenter;
	private int mRadius;
	private float cosOfHalfAngleSpan = 0;
	private float sinOfHalfAngleSpan = 0;
	private int initialSelectedItem=-1;
	public CarouselWithAdapter(Context context, AttributeSet attrs, int defStyle) {
		
		super(context, attrs, defStyle);
		
        setFocusable(true);
        setWillNotDraw(false);
		// It's needed to make items with greater value of
		// z coordinate to be behind items with lesser z-coordinate
		setChildrenDrawingOrderEnabled(true);
		
		// Making user gestures available 
		mGestureDetector = new GestureDetector(this);
		mGestureDetector.setIsLongpressEnabled(true);
		
		// It's needed to apply 3D transforms to items
		// before they are drawn
		setStaticTransformationsEnabled(false);
		
		// Retrieve settings
		TypedArray arr = getContext().obtainStyledAttributes(attrs, R.styleable.Carousel);
		mAnimationDuration = arr.getInteger(R.styleable.Carousel_android_animationDuration, defaultAnimationDuration);
		mUseReflection = arr.getBoolean(R.styleable.Carousel_UseReflection, false);	
		initialSelectedItem = arr.getInteger(R.styleable.Carousel_SelectedItem, -1);
		mItemsToShowCount = arr.getInteger(R.styleable.Carousel_itemsToShowCount, 5);
		
		int min = arr.getInteger(R.styleable.Carousel_minQuantity, MIN_QUANTITY);
		int max = arr.getInteger(R.styleable.Carousel_maxQuantity, MAX_QUANTITY);
		
		mAngleSpan = arr.getInteger(R.styleable.Carousel_angleSpan, 0);
		int t = arr.getInt(R.styleable.Carousel_affinity, 0);
		mAffinity = (t==0) ? CarouselAffinity.LEFT:
		                     (t==1) ? CarouselAffinity.RIGHT:
		                              (t==2) ? CarouselAffinity.BOTTOM: CarouselAffinity.TOP;

		mCal = Calendar.getInstance();
		mStartAngle = (getBaselineAngle(mAffinity) - mAngleSpan/2);
		mEndAngle = mStartAngle + mAngleSpan;
		cosOfHalfAngleSpan = (getBaselineAngle(mAffinity) - mStartAngle);
		cosOfHalfAngleSpan = (float) (cosOfHalfAngleSpan * (Math.PI/180.0f));
		sinOfHalfAngleSpan = (float) Math.sin(cosOfHalfAngleSpan);
		cosOfHalfAngleSpan = (float) Math.cos(cosOfHalfAngleSpan);
		float mTheta = arr.getFloat(R.styleable.Carousel_maxTheta, (float)MAX_THETA);
		if(mTheta > MAX_THETA || mTheta < 0.0f)
			mTheta = MAX_THETA;
		
		mMinQuantity = min < MIN_QUANTITY ? MIN_QUANTITY : min;
		mMaxQuantity = max > MAX_QUANTITY ? MAX_QUANTITY : max;
		
		if(arr.length() < mMinQuantity || arr.length() > mMaxQuantity)
			throw new IllegalArgumentException("Invalid set of items.");
								
		// Initialize image adapter
		CarouselAdapter adapter = new CarouselAdapter(getContext());
		int imageArrayID = arr.getResourceId(R.styleable.Carousel_Items, -1);		
		TypedArray images = null;
		if (imageArrayID != -1) {
			images = getResources().obtainTypedArray(imageArrayID);
			adapter.SetImages(images, mUseReflection);
		}

		setAdapter(adapter);

        int middle = initialSelectedItem ;;
	    if(initialSelectedItem < 0 || initialSelectedItem >= adapter.getCount())
        	initialSelectedItem = adapter.getCount()/2;

	    // next time we go through layout with this value
        setNextSelectedPositionInt(initialSelectedItem);
        
        arr.recycle();
        this.setClipToPadding(false);
        mCenter = findCenter();
	}
	
	public void SetImages(int[] drawable_ids) {
	    if (getAdapter() != null) {
	    	getAdapter().SetImages(drawable_ids);
	    }
	    resetList();
	    this.handleDataChanged();
	    this.postInvalidateDelayed(1000);
	    forceLayout();
	}
	
	public void setAdapter(CarouselAdapter adapter) {
        if (null != mAdapter) {
            mAdapter.unregisterDataSetObserver(mDataSetObserver);
            resetList();
        }
        
        mAdapter = adapter;
        
        mOldSelectedPosition = INVALID_POSITION;
        mOldSelectedRowId = INVALID_ROW_ID;
        
        if (mAdapter != null) {
            mOldItemCount = mItemCount;
            mItemCount = mAdapter.getCount();
            checkFocus();

            mDataSetObserver = new AdapterDataSetObserver();
            //mAdapter.registerDataSetObserver(mDataSetObserver);

            int position = mItemCount > 0 ? 0 : INVALID_POSITION;
            position = mItemCount > 0 ? mItemCount/2: INVALID_POSITION;

            setSelectedPositionInt1(position);
            setNextSelectedPositionInt(position);
            
            if (mItemCount == 0) {
                // Nothing selected
                checkSelectionChanged();
            }
            
        } else {
            checkFocus();            
            resetList();
            // Nothing selected
            checkSelectionChanged();
        }

        requestLayout();
	}

    /**
     * Clear out all children from the list
     */
    void resetList() {
        mDataChanged = false;
        mNeedSync = false;
        
        removeAllViewsInLayout();
        mOldSelectedPosition = INVALID_POSITION;
        mOldSelectedRowId = INVALID_ROW_ID;
        
        setSelectedPositionInt1(INVALID_POSITION);
        setNextSelectedPositionInt(INVALID_POSITION);
        invalidate();
    }
		
	// [end]
		
	// [start] View overrides
	
	// [start] These are for use with horizontal scrollbar
	
	/**
	 * Compute the horizontal extent of the horizontal scrollbar's thumb 
	 * within the horizontal range. This value is used to compute 
	 * the length of the thumb within the scrollbar's track.
	 */
    @Override
    protected int computeHorizontalScrollExtent() {
        // Only 1 item is considered to be selected
        return 1;
    }
    
    /**
     * Compute the horizontal offset of the horizontal scrollbar's 
     * thumb within the horizontal range. This value is used to compute 
     * the position of the thumb within the scrollbar's track.
     */
    @Override
    protected int computeHorizontalScrollOffset() {
        // Current scroll position is the same as the selected position
        return mSelectedPosition;
    }
    
    
    /**
     * Compute the horizontal range that the horizontal scrollbar represents.
     */
    @Override
    protected int computeHorizontalScrollRange() {
        // Scroll range is the same as the item count
        return mItemCount;
    }
    
    // [end]
    
   
    /**
     * Extra information about the item for which the context menu should be shown.
     */
    @Override
    protected ContextMenuInfo getContextMenuInfo() {
        return mContextMenuInfo;
    }    

    /**
     * Bring up the context menu for this view.
     */
    @Override
    public boolean showContextMenu() {
        
        if (isPressed() && mSelectedPosition >= 0) {
            int index = mSelectedPosition - mFirstPosition;
            View v = getChildAt(index);
            return dispatchLongPress(v, mSelectedPosition, mSelectedRowId);
        }        
        
        return false;
    }

	// [end]
	
    // [start] ViewGroup overrides
    
    @Override
    protected boolean checkLayoutParams(ViewGroup.LayoutParams p) {
        return p instanceof LayoutParams;
    }
    
    @Override
    protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams p) {
        return new LayoutParams(p);
    }
    
    @Override
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet attrs) {
        return new LayoutParams(getContext(), attrs);
    }
        
    @Override
    public void dispatchSetSelected(boolean selected) {
        /*
         * We don't want to pass the selected state given from its parent to its
         * children since this widget itself has a selected state to give to its
         * children.
         */
    }
    
    @Override
    protected void dispatchSetPressed(boolean pressed) {
        
        // Show the pressed state on the selected child
        if (mSelectedChild != null) {
            mSelectedChild.setPressed(pressed);
        }
    }
    
    @Override
    public boolean showContextMenuForChild(View originalView) {

        final int longPressPosition = getPositionForView(originalView);
        if (longPressPosition < 0) {
            return false;
        }
        
        final long longPressId = mAdapter.getItemId(longPressPosition);
        return dispatchLongPress(originalView, longPressPosition, longPressId);
    }
        
        
    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        // Gallery steals all key events
        return event.dispatch(this);
    }
        
    /**
     * Index of the child to draw for this iteration
     */
    @Override
    protected int getChildDrawingOrder(int childCount, int i) {
    	return super.getChildDrawingOrder(childCount, i);
    	//return getChildDrawingOrder_new(childCount, i);
    }
    
    
    protected int getChildDrawingOrder_new(int childCount, int i) {
    	int centerY = getHeight()/2;
    	int ii=0;
    	int minY=getHeight();;
    	int tmpY;
    	
    	//find minimum distance from beaseline
    	for(int j = 0; j < childCount; j++) {
    		CarouselView view = getAdapter().getView(j,null, null);
    		tmpY = Math.abs((int) view.getYPosition() - centerY);
    		if (tmpY < minY) {
    			minY = tmpY;
    			ii = j;
    		}
    	}
    	if (i == ii)
    		return 0;
    	if (i==0)
    		return ii;
    	return i;

    }
    protected int getChildDrawingOrder_old(int childCount, int i) {
    	// Sort Carousel items by z coordinate in reverse order
    	ArrayList<CarouselView> sl = new ArrayList<CarouselView>();
    	for(int j = 0; j < childCount; j++)
    	{
    		CarouselView view = getAdapter().getView(j,null, null);
    		if(i == 0)
    			view.setDrawn(false);
    		//sl.add((CarouselView)getAdapter().getView(j,null, null));
    		sl.add(view);
    	}

    	Collections.sort(sl);
    	
    	// Get first undrawn item in array and get result index
    	int idx = 0;
    	
    	for(CarouselView civ : sl)
    	{
    		if(!civ.isDrawn())
    		{
    			civ.setDrawn(true);
    			idx = civ.getIndex();
    			break;
    		}
    	}
    	
    	return idx;

    }
    
    float startTouchX, startTouchY;



    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouchEvent_test(MotionEvent event) {
        Log.d(TAG, "onTouch");
        // Give everything to the gesture detector
        boolean retValue = true;
        int action = event.getAction();
        //retValue = super.onTouchEvent(event);
        if (action == MotionEvent.ACTION_DOWN) {
             startTouchX=event.getX();
             startTouchY=event.getY();
             getParent().requestDisallowInterceptTouchEvent(true);
        }
       
        if (action == MotionEvent.ACTION_MOVE) {
        	float angle = getAngle(startTouchX, event.getX(), startTouchY, event.getY());
        	if (event.getY() >startTouchY)
        		trackMotionScroll(Math.abs(angle/15));
        	else
        		trackMotionScroll(-Math.abs(angle/15));
             startTouchX=event.getX();
             startTouchY=event.getY();
        }

        if (action == MotionEvent.ACTION_UP) {
            // Helper method for lifted finger
            onUp();
        } else if (action == MotionEvent.ACTION_CANCEL) {
            onCancel();
        }
        
        return retValue;
    }	
    

    @SuppressLint("ClickableViewAccessibility")
	@Override
    public boolean onTouchEvent(MotionEvent event) {
        //Log.d(TAG, "onTouch");
        boolean retValue = true;
        retValue = super.onTouchEvent(event);
        // Give everything to the gesture detector
        retValue = mGestureDetector.onTouchEvent(event);

        int action = event.getAction();
        if (action == MotionEvent.ACTION_UP) {
            // Helper method for lifted finger
            onUp();
        } else if (action == MotionEvent.ACTION_CANCEL) {
            onCancel();
        }
        
        return retValue;
    }	    
 
    
    /**
     * Transform an item depending on it's coordinates  
     */
	//@Override
	protected boolean getChildStaticTransformation_old(View child, Transformation transformation) {

		transformation.clear();
		transformation.setTransformationType(Transformation.TYPE_MATRIX);
		
		// Center of the item
		float centerX = (float)child.getWidth()/2, centerY = (float)child.getHeight()/2;
		
		// Save camera
		mCamera.save();
		
		// Translate the item to it's coordinates
		final Matrix matrix = transformation.getMatrix();
		
		//Log.d(TAG, "index="+((CarouselView)child).getIndex()+
		              //",x="+((CarouselView)child).getXPosition()+
				      //",y="+((CarouselView)child).getYPosition()+
				      //",z="+((CarouselView)child).getZPosition()) ;


		float x = (float) ((CarouselView) child).getXPosition();
		float y = (float) ((CarouselView) child).getYPosition();
		float z = (float) ((CarouselView) child).getZPosition();
		mCamera.translate(x,y,z);

		// Align the item
		mCamera.getMatrix(matrix);
		matrix.preTranslate(-centerX, -centerY);
		matrix.postTranslate(centerX, centerY);
		
		// Restore camera
		mCamera.restore();		
			
		/*
		if (((CarouselView)child).getXPosition() < 0)
		    child.setPressed(false);
		else
		    child.setPressed(true);
		    */
		
		return true;
	}     
	 
    /**
     * Transform an item depending on it's coordinates  
     */
	@Override
	protected boolean getChildStaticTransformation(View child, Transformation transformation) {

		transformation.clear();
		transformation.setTransformationType(Transformation.TYPE_MATRIX);
		
		// Center of the item
		float centerX = (float)child.getWidth()/2, centerY = (float)child.getHeight()/2;
		
		// Translate the item to it's coordinates
		final Matrix matrix = transformation.getMatrix();
		
		
		//Log.d(TAG, "index="+((CarouselView)child).getIndex()+
		              //",x="+((CarouselView)child).getXPosition()+
				      //",y="+((CarouselView)child).getYPosition()+
				      //",z="+((CarouselView)child).getZPosition()) ;


		float x = (float) ((CarouselView) child).getXPosition();
		float y = (float) ((CarouselView) child).getYPosition();
		float z = (float) ((CarouselView) child).getZPosition();
		
		if ((mAffinity == CarouselAffinity.LEFT)||
		     (mAffinity == CarouselAffinity.RIGHT)) {
			x = x - child.getWidth()/2;
		}
		if ((mAffinity == CarouselAffinity.TOP)||
		     (mAffinity == CarouselAffinity.BOTTOM)) {
			y = y - child.getHeight()/2;
		}
		
		//mCamera.save();
		//mCamera.translate(x,y,0);
		//mCamera.getMatrix(matrix);
		//mCamera.restore();		

		matrix.setTranslate(x,y);

		
		return true;
	}     
		
	
    /** 
     * @see android.view.View#measure(int, int)
     * 
     * Figure out the dimensions of this Spinner. The width comes from
     * the widthMeasureSpec as Spinnners can't have their width set to
     * UNSPECIFIED. The height is based on the height of the selected item
     * plus padding. 
     */
    @SuppressLint("NewApi")
	@Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    	Log.d(TAG, "onMeasure(widthMeasureSpec: " + widthMeasureSpec
				+ " heightMeasureSpec: " + heightMeasureSpec + ")");
        int widthSize;
        int heightSize;
        //Log.d(TAG, "widthMeasureSpec="+View.MeasureSpec.toString(widthMeasureSpec));
        //Log.d(TAG, "heightMeasureSpec="+View.MeasureSpec.toString(heightMeasureSpec));

        if (mDataChanged) {
            handleDataChanged();
        }
        
        int preferredHeight = 0;
        int preferredWidth = 0;
        mRecycler.clear();
        
       	CarouselView view;
        for(int i = 0; i < getAdapter().getCount(); i++) {
        	view = (((CarouselView)getAdapter().getView(i, null, null)));
        	view.setLayoutParams(generateDefaultLayoutParams());
        	if (view.getLayoutParams() == null) {
        		mBlockLayoutRequests = true;
        		view.setLayoutParams(generateDefaultLayoutParams());
        		mBlockLayoutRequests = false;
        	}
        	measureChild(view, widthMeasureSpec, heightMeasureSpec);

        	if (preferredHeight < view.getMeasuredHeight())
        		preferredHeight = view.getMeasuredHeight();
        	if (preferredWidth < view.getMeasuredWidth())
        		preferredWidth = view.getMeasuredWidth();
        }

        preferredHeight = Math.max(preferredHeight, getSuggestedMinimumHeight());
        preferredWidth = Math.max(preferredWidth, getSuggestedMinimumWidth());
        //Log.d(TAG, "preferredWidth="+preferredWidth+",preferredHeight="+preferredHeight);

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB){
        	int childMeasuredState = 0;
        	childMeasuredState = resolveSizeAndState(preferredHeight, heightMeasureSpec, childMeasuredState);
        	heightSize = childMeasuredState & View.MEASURED_SIZE_MASK ;
        	childMeasuredState = (childMeasuredState & View.MEASURED_STATE_MASK);
        	if (childMeasuredState == View.MEASURED_STATE_TOO_SMALL) {
        		//Log.w(TAG, "the given height from parent is too small");
        	}

        	childMeasuredState = 0;
        	childMeasuredState = resolveSizeAndState(preferredWidth, widthMeasureSpec, childMeasuredState);
        	widthSize = childMeasuredState & View.MEASURED_SIZE_MASK ;
        	childMeasuredState = (childMeasuredState & View.MEASURED_STATE_MASK);
        	if (childMeasuredState == View.MEASURED_STATE_TOO_SMALL) {
        		//Log.w(TAG, "the given width from parent is too small");
        	}
        } else{
        	heightSize = resolveSize(preferredHeight, heightMeasureSpec);
        	widthSize = resolveSize(preferredWidth, widthMeasureSpec);
        }
        
        //heightSize = preferredHeight;
        //widthSize = preferredWidth;

        //Log.d(TAG,"mSpinnerPadding="+mSpinnerPadding);
        //Log.d(TAG, "widthSize="+widthSize+",heightSize="+heightSize);

        //setMeasuredDimension(View.MeasureSpec.getSize(widthMeasureSpec), heightSize);
        //setMeasuredDimension(widthSize, heightSize);
        setMeasuredDimension(preferredWidth+LIP_WIDTH + getPaddingLeft()+getPaddingRight(), heightSize);
        //Log.d(TAG, "myMeasuredWidth="+getMeasuredWidth()+",myMeasuredHeight="+getMeasuredHeight());
        mHeightMeasureSpec = heightMeasureSpec;
        mWidthMeasureSpec = widthMeasureSpec;
        mCenter = findCenter();
    }
    
	
    /** 
     * @see android.view.View#measure(int, int)
     * 
     * Figure out the dimensions of this Spinner. The width comes from
     * the widthMeasureSpec as Spinnners can't have their width set to
     * UNSPECIFIED. The height is based on the height of the selected item
     * plus padding. 
     */
    @SuppressLint("NewApi")
    protected void onMeasure_old(int widthMeasureSpec, int heightMeasureSpec) {
        //int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        int widthSize;
        int heightSize;
        //Log.d(TAG, "widthMeasureSpec="+View.MeasureSpec.toString(widthMeasureSpec));
        //Log.d(TAG, "heightMeasureSpec="+View.MeasureSpec.toString(heightMeasureSpec));

        if (mDataChanged) {
            handleDataChanged();
        }
        
        int preferredHeight = 0;
        int preferredWidth = 0;
        boolean needsMeasuring = true;
        
        int selectedPosition = getSelectedItemPosition();
        if (selectedPosition >= 0 && mAdapter != null && selectedPosition < mAdapter.getCount()) {
            // Try looking in the recycler. (Maybe we were measured once already)
            View view = mRecycler.get(selectedPosition);
            if (view == null) {
                // Make a new one
                view = mAdapter.getView(selectedPosition, null, this);
            }

            if (view != null) {
                // Put in recycler for re-measuring and/or layout
                mRecycler.put(selectedPosition, view);
            }

            if (view != null) {
                    view.setLayoutParams(generateDefaultLayoutParams());
                if (view.getLayoutParams() == null) {
                    mBlockLayoutRequests = true;
                    view.setLayoutParams(generateDefaultLayoutParams());
                    mBlockLayoutRequests = false;
                }
                measureChild(view, widthMeasureSpec, heightMeasureSpec);
                
                preferredHeight = view.getMeasuredHeight();
                preferredWidth = view.getMeasuredWidth();
                //Log.d(TAG,"child-"+view+" preferredHeight="+ preferredHeight+",preferredWidth="+preferredWidth);
                
                needsMeasuring = false;
                //Log.d(TAG,"needsMeasuring="+needsMeasuring);
            }
        }
        

        preferredHeight = Math.max(preferredHeight, getSuggestedMinimumHeight());
        preferredWidth = Math.max(preferredWidth, getSuggestedMinimumWidth());
        //Log.d(TAG, "preferredWidth="+preferredWidth+",preferredHeight="+preferredHeight);

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.HONEYCOMB){
        	int childMeasuredState = 0;
        	childMeasuredState = resolveSizeAndState(preferredHeight, heightMeasureSpec, childMeasuredState);
        	heightSize = childMeasuredState & View.MEASURED_SIZE_MASK ;
        	childMeasuredState = (childMeasuredState & View.MEASURED_STATE_MASK);
        	if (childMeasuredState == View.MEASURED_STATE_TOO_SMALL) {
        		//Log.w(TAG, "the given height from parent is too small");
        	}

        	childMeasuredState = 0;
        	childMeasuredState = resolveSizeAndState(preferredWidth, widthMeasureSpec, childMeasuredState);
        	widthSize = childMeasuredState & View.MEASURED_SIZE_MASK ;
        	childMeasuredState = (childMeasuredState & View.MEASURED_STATE_MASK);
        	if (childMeasuredState == View.MEASURED_STATE_TOO_SMALL) {
        		//Log.w(TAG, "the given width from parent is too small");
        	}
        } else{
        	heightSize = resolveSize(preferredHeight, heightMeasureSpec);
        	widthSize = resolveSize(preferredWidth, widthMeasureSpec);
        }
        
        //heightSize = preferredHeight;
        //widthSize = preferredWidth;

        //Log.d(TAG,"mSpinnerPadding="+mSpinnerPadding);
        //Log.d(TAG, "widthSize="+widthSize+",heightSize="+heightSize);

        //setMeasuredDimension(View.MeasureSpec.getSize(widthMeasureSpec), heightSize);
        //setMeasuredDimension(widthSize, heightSize);
        setMeasuredDimension(preferredWidth+LIP_WIDTH + getPaddingLeft()+getPaddingRight(), heightSize);
        //Log.d(TAG, "myMeasuredWidth="+getMeasuredWidth()+",myMeasuredHeight="+getMeasuredHeight());
        mHeightMeasureSpec = heightMeasureSpec;
        mWidthMeasureSpec = widthMeasureSpec;
        mCenter = findCenter();
    }
    
    
    @Override
    protected ViewGroup.LayoutParams generateDefaultLayoutParams() {
        /* Carousel expects Carousel.LayoutParams. */
        return new CarouselWithAdapter.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
    }
    
    void recycleAllViews() {
        final int childCount = getChildCount();
        final RecycleBin recycleBin = mRecycler;
        final int position = mFirstPosition;

        // All views go in recycler
        for (int i = 0; i < childCount; i++) {
            View v = getChildAt(i);
            int index = position + i;
            recycleBin.put(index, v);
        }  
    }    
    
    /**
     * Override to prevent spamming ourselves with layout requests
     * as we place views
     * 
     * @see android.view.View#requestLayout()
     */
    @Override
    public void requestLayout() {
        if (!mBlockLayoutRequests) {
            super.requestLayout();
        }
    }
    

    /**
     * @return The number of items owned by the Adapter associated with this
     *         CarouselAdapter. (This is the number of data items, which may be
     *         larger than the number of visible view.)
     */
    @ViewDebug.CapturedViewProperty
    public int getCount() {
        return mItemCount;
    }    


    public int getItemsToShowCount() {
        return mItemsToShowCount;
    }    
    // [end]
	
    // [start] CarouselAdapter overrides
    
	/**
	 * Setting up images
	 */
	void layoutMe(int delta, boolean animate){
		        
        if (mDataChanged) {
            handleDataChanged();
        }
        
        // Handle an empty gallery by removing all views.
        if (getCount() == 0) {
            resetList();
            return;
        }
        
        // Update to the new selected position.
        if (mNextSelectedPosition >= 0) {
            setSelectedPositionInt(mNextSelectedPosition);
        }        
        
        // All views go in recycler while we are in layout
        recycleAllViews();        
        
        // Clear out old views
        detachAllViewsFromParent();
        
        
        int showCount = mItemsToShowCount;
        int count = getCount();
        //float angleUnit = 360.0f / count;
        float angleUnit = (mEndAngle - mStartAngle) / showCount;

       	float angle = this.getBaselineAngle(mAffinity);
       	angle = angle - initialSelectedItem * angleUnit;
        //for(int i = 0; i<getAdapter().getCount(); i++){
        for(int i=0; i<=initialSelectedItem; i++){
        	if(angle < 0.0f)
        		angle = 360.0f + angle;
           	makeAndAddView(i, angle);        	
        	angle = angle + angleUnit;
        }

       	angle = this.getBaselineAngle(mAffinity);
        for(int i = initialSelectedItem+1; i < count; i++){
        	angle = angle + angleUnit;
        	if(angle < 0.0f)
        		angle = 360.0f + angle;
           	makeAndAddView(i, angle);        	
        }

        // Flush any cached views that did not get reused above
        mRecycler.clear();

        invalidate();

        setNextSelectedPositionInt(mSelectedPosition);
        
        checkSelectionChanged();
        
        ////////mDataChanged = false;
        mNeedSync = false;
        
        updateSelectedItemMetadata();
	} 

    
    
	/**
	 * Setting up images after layout changed
	 */
    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    	Log.d(TAG, "onLayout(changed:"+changed+" l:"+l+"t: "+t+" r:"+r+" b:"+b+")");
        mLayoutHeight = getHeight();
        
        /*
         * Remember that we are in layout to prevent more layout request from
         * being generated.
         */
        mInLayout = true;
        layoutMe(0, false);

        mInLayout = false;
    }	    
    	

      
    // [end]
    
    // [start] Rotation class for the Carousel    
	
	private class FlingRotateRunnable implements Runnable {

        /**
         * Tracks the decay of a fling rotation
         */		
		private Rotator mRotator;

		/**
         * Angle value reported by mRotator on the previous fling
         */
        private float mLastFlingAngle;
        
        /**
         * Constructor
         */
        public FlingRotateRunnable(){
        	mRotator = new Rotator(getContext());
        }
        
        private void startCommon() {
            // Remove any pending flings
            removeCallbacks(this);
        }
        
        public void startUsingVelocity(float initialVelocity) {
            if (initialVelocity == 0) return;
            
            startCommon();
                        
            mLastFlingAngle = 0.0f;
            
           	mRotator.fling(initialVelocity);
                        
            //postDelayed(this, 200);
            post(this);
        }        
        
        
        public void startUsingDistance(float deltaAngle) {
            if (deltaAngle == 0) return;
            
            startCommon();
            
            mLastFlingAngle = 0;
            synchronized(this)
            {
            	mRotator.startRotate(0.0f, -deltaAngle, mAnimationDuration);
            }
            //postDelayed(this,200);
            post(this);
        }
        
        public void stop(boolean scrollIntoSlots) {
            removeCallbacks(this);
            endFling(scrollIntoSlots);
        }        
        
        private void endFling(boolean scrollIntoSlots) {

        	//Log.d(TAG, "endFling(scrollIntoSlots: " + scrollIntoSlots + ")");
            /*
             * Force the scroller's status to finished (without setting its
             * position to the end)
             */
        	synchronized(this){
        		mRotator.forceFinished(true);
        	}
            
            if (scrollIntoSlots) scrollIntoSlots();
        }
                		
		public void run() {
            if (CarouselWithAdapter.this.getChildCount() == 0) {
                endFling(true);
                return;
            }			
            
            mShouldStopFling = false;
            
            final Rotator rotator;
            final float angle;
            boolean more;
            synchronized(this){
	            rotator = mRotator;
	            more = rotator.computeAngleOffset();
	            angle = rotator.getCurrAngle();	            
            }
            //Log.d(TAG,"FlingRotateRunnable: angle="+angle);
            
            
            float delta = mLastFlingAngle - angle;                        
            
            //////// Should be reworked
            trackMotionScroll(delta);
            
            if (more && !mShouldStopFling) {
                mLastFlingAngle = angle;
                //postDelayed(this,200);
                post(this);
            } else {
                mLastFlingAngle = 0.0f;
                endFling(true);
            }            
		}
	}
	
	public Point getCenter() {
		return mCenter;
	}
	public Point findCenter() {
        mRadius = findRadius(mAngleSpan, getMeasuredHeight());
		Point point = new Point();
		float k = Math.abs(cosOfHalfAngleSpan * mRadius);
		if (mAffinity== CarouselAffinity.RIGHT)
			point.x = (int) (getRight() +k);
		if (mAffinity== CarouselAffinity.LEFT)
			point.x = (int) (-k);
		point.y = (int) Math.abs(sinOfHalfAngleSpan * mRadius);
		
		return point;
	}
	

	boolean hidden = false;
	@SuppressLint("NewApi")
	public void toggleHidden() {
		if (!hidden) {
			hidden=true;
			if (mAffinity == CarouselAffinity.LEFT) {
				//this.scrollBy(getWidth()-100, 0);
				this.animate().translationXBy(getWidth()-LIP_WIDTH).setDuration(1700);
			}
			if (mAffinity == CarouselAffinity.RIGHT) {
				//this.scrollBy(-getWidth()+100, 0);
				this.animate().translationXBy(getWidth()-LIP_WIDTH).setDuration(1700);
			}
		} else {
			hidden=false;
			if (mAffinity == CarouselAffinity.LEFT) {
				//this.scrollBy(-getWidth()+100, 0);
				this.animate().translationXBy(getWidth()-LIP_WIDTH).setDuration(1700);
			}
			if (mAffinity == CarouselAffinity.RIGHT) {
				//this.scrollBy(getWidth()-100, 0);
				this.animate().translationXBy(-getWidth()+LIP_WIDTH).setDuration(1700);
			}
		}
	}
	
	/**
     * Maps a point to a position in the list.
     * 
     * @param x X in local coordinate
     * @param y Y in local coordinate
     * @return The position of the item which contains the specified point, or
     *         {@link #INVALID_POSITION} if the point does not intersect an item.
     */
    public int determinePosition(MotionEvent e) {    	
        int x = (int) e.getX();
        int y = (int) e.getY();
    	Rect r = new Rect();
    	//Rect r1 = new Rect();
        //Log.d(TAG,"x="+x+",y="+y);
    	for (int i = getChildCount() - 1; i >= 0; i--) {
            CarouselView cv = (CarouselView)getChildAt(i);
            cv.getHitRect(r);
            //cv.getHitRect(r1);
            //this.getChildVisibleRect(cv, r1, null);
            //Log.d(TAG,"index="+i+",hitRect="+r+",currentAngle="+cv.getCurrentAngle());
            //Log.d(TAG,"index="+i+",hitRect1="+r1);
            //Log.d(TAG," x="+cv.getXPosition()+ ",y="+cv.getYPosition()+ ",z="+cv.getZPosition()) ;
            if (r.contains(x, y)) {
            	//Log.d(TAG, "index="+cv.getIndex()+" is HIT");
            	//Log.d(TAG, "mSelectedPosition="+mSelectedPosition);
            	//cv.onTouchEvent(e);
            	return cv.getIndex();
            }
        }
        
    	// All touch events are applied to selected item
    	return INVALID_POSITION;
    }
 	
	/**
     * Maps a point to a position in the list.
     * 
     * @param x X in local coordinate
     * @param y Y in local coordinate
     * @return The position of the item which contains the specified point, or
     *         {@link #INVALID_POSITION} if the point does not intersect an item.
     */
    public boolean determineLipHit(MotionEvent e) {    	
        int x = (int) e.getX();
        int y = (int) e.getY();
    	Rect r = new Rect();
        //Log.d(TAG,"x="+x+",y="+y);
    	for (int i = getChildCount() - 1; i >= 0; i--) {
            CarouselView cv = (CarouselView)getChildAt(i);
            //Log.d(TAG,"index="+i+",currentAngle="+cv.getCurrentAngle());
            int angle = (int)Math.round(cv.getCurrentAngle());
            if (angle == getBaselineAngle(mAffinity)) {
            	cv.getHitRect(r);
            	r.right= r.left;
            	r.left = r.left - LIP_WIDTH;
            	r.right= r.left + LIP_WIDTH;
            	//Log.d(TAG,"hitRectLip="+r);
            	if (r.contains(x, y)) {
            		//Log.d(TAG, "LIP is HIT");
            		return true;
            	}
            }
        }
        
    	return false;
    }
       
    static class SavedState extends BaseSavedState {
        long selectedId;
        int position;

        /**
         * Constructor called from {@link AbsSpinner#onSaveInstanceState()}
         */
        SavedState(Parcelable superState) {
            super(superState);
        }
        
        /**
         * Constructor called from {@link #CREATOR}
         */
        private SavedState(Parcel in) {
            super(in);
            selectedId = in.readLong();
            position = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel out, int flags) {
            super.writeToParcel(out, flags);
            out.writeLong(selectedId);
            out.writeInt(position);
        }

        @Override
        public String toString() {
            return "AbsSpinner.SavedState{"
                    + Integer.toHexString(System.identityHashCode(this))
                    + " selectedId=" + selectedId
                    + " position=" + position + "}";
        }

        public static final Parcelable.Creator<SavedState> CREATOR
                = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState ss = new SavedState(superState);
        ss.selectedId = getSelectedItemId();
        if (ss.selectedId >= 0) {
            ss.position = getSelectedItemPosition();
        } else {
            ss.position = INVALID_POSITION;
        }
        return ss;
    }    

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        SavedState ss = (SavedState) state;
  
        super.onRestoreInstanceState(ss.getSuperState());

        if (ss.selectedId >= 0) {
            mDataChanged = true;
            mNeedSync = true;
            mSyncRowId = ss.selectedId;
            mSyncPosition = ss.position;
            mSyncMode = SYNC_SELECTED_POSITION;
            requestLayout();
        }
    }
    
	
	// [end]
	
	// [start] OnGestureListener implementation
	
	public boolean onDown(MotionEvent e) {
        // Kill any existing fling/scroll
        mFlingRunnable.stop(false);

        ///// Don't know yet what for it is
        // Get the item's view that was touched
        mDownTouchPosition = determinePosition(e);
		//Log.d(TAG, "mDownTouchPosition="+mDownTouchPosition);
        
        if (mDownTouchPosition >= 0) {
            mDownTouchView = getChildAt(mDownTouchPosition - mFirstPosition);
            mDownTouchView.setPressed(true);
        }
        
        // Reset the multiple-scroll tracking state
        mIsFirstScroll = true;
        
        // Must return true to get matching events for this down event.
        return true;
	}	
	
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		
        if (!mShouldCallbackDuringFling) {
            // We want to suppress selection changes
            
            // Remove any future code to set mSuppressSelectionChanged = false
            removeCallbacks(mDisableSuppressSelectionChangedRunnable);

            // This will get reset once we scroll into slots
            if (!mSuppressSelectionChanged) mSuppressSelectionChanged = true;
        }
        
        // Fling the gallery!
        //mFlingRunnable.startUsingVelocity((int) -velocityX);
        mFlingRunnable.startUsingVelocity((int) -velocityY/16);
        
        return true;
    }
					
	
	public void onLongPress(MotionEvent e) {
        
        if (mDownTouchPosition < 0) {
            return;
        }
        
        performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
        long id = getItemIdAtPosition(mDownTouchPosition);
        dispatchLongPress(mDownTouchView, mDownTouchPosition, id);
        
	}
			
	
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {

        //Log.v(TAG, "x-"+String.valueOf(e2.getX() - e1.getX()));
        //Log.v(TAG, "y:"+e2.getY() +","+ e1.getY()+ ",distanceY:"+distanceY);
        
        /*
         * Now's a good time to tell our parent to stop intercepting our events!
         * The user has moved more than the slop amount, since GestureDetector
         * ensures this before calling this method. Also, if a parent is more
         * interested in this touch's events than we are, it would have
         * intercepted them by now (for example, we can assume when a Gallery is
         * in the ListView, a vertical scroll would not end up in this method
         * since a ListView would have intercepted it by now).
         */
        
        getParent().requestDisallowInterceptTouchEvent(true);
        
        // As the user scrolls, we want to callback selection changes so related-
        // info on the screen is up-to-date with the gallery's selection
        if (!mShouldCallbackDuringFling) {
            if (mIsFirstScroll) {
                /*
                 * We're not notifying the client of selection changes during
                 * the fling, and this scroll could possibly be a fling. Don't
                 * do selection changes until we're sure it is not a fling.
                 */
                if (!mSuppressSelectionChanged) mSuppressSelectionChanged = true;
                postDelayed(mDisableSuppressSelectionChangedRunnable, SCROLL_TO_FLING_UNCERTAINTY_TIMEOUT);
            }
        } else {
            if (mSuppressSelectionChanged) mSuppressSelectionChanged = false;
        }
        
        float angle = getAngle(e1.getX(), e2.getX(), e1.getY(), e2.getY());
        //Log.d(TAG,"angle="+angle);
        if (distanceY<0)
        	trackMotionScroll(Math.abs(angle/15));
        else
        	trackMotionScroll(-Math.abs(angle/15));
       
        mIsFirstScroll = false;
        return true;
     }
	
	
	public float getAngle(float x1, float x2,  float y1, float y2) {
		//Log.d(TAG, "x1:"+x1+" y1:"+y1+" x2:"+x2+" y2:"+y2+")");
		//Log.i(TAG,"center is at="+mCenter);
		x1=x1-mCenter.x;
		x2=x2-mCenter.x;
		y1=y1-mCenter.y;
		y2=y2-mCenter.y;

		//Log.d(TAG, "x1:"+x1+" y1:"+y1+" x2:"+x2+" y2:"+y2+")");
		float angle = (float) Math.atan(-y2/ x2);
		//Log.i(TAG,"angle is at="+angle);
		float angle2 = (float) Math.atan(-y1/ x1);
		//Log.i(TAG,"angle2 is at="+angle2);
		angle = angle - angle2;
		//Log.i(TAG,"diff angle="+angle);
		angle = (float) (angle *180.0f/Math.PI);
		//Log.i(TAG,"degree diff angle="+angle);
		return  (angle);
	}
	
	
	public boolean onSingleTapUp(MotionEvent e) {
		//Log.d(TAG, "mDownTouchPosition="+mDownTouchPosition+",mFirstPosition="+mFirstPosition);
		if (determineLipHit(e))
			toggleHidden();
        if (mDownTouchPosition >= 0) {
            
            // An item tap should make it selected, so scroll to this child.
            scrollToChild(mDownTouchPosition - mFirstPosition);

            // Also pass the click so the client knows, if it wants to.
            if (mShouldCallbackOnUnselectedItemClick || mDownTouchPosition == mSelectedPosition) {
                performItemClick(mDownTouchView, mDownTouchPosition, mAdapter
                        .getItemId(mDownTouchPosition));
            }
            
            return true;
        }
        return false;
    }
		
	
	// Unused gestures
	public void onShowPress(MotionEvent e) {
	}

    private void calculate3DPosition_orig(CarouselView child, int diameter, float angleOffset){
		//Log.d(TAG, "diameter="+diameter) ;
    	angleOffset = angleOffset * (float)(Math.PI/180.0f);
    	
    	float x = -(float)(diameter/2*Math.sin(angleOffset));
    	float z = diameter/2 * (1.0f - (float)Math.cos(angleOffset));
    	float y = - getHeight()/2 + (float) (z * Math.sin(mTheta));
    	
		//Log.d(TAG, "x="+x+",y="+y+",z="+z) ;
    	child.setXPosition(x);
    	child.setZPosition(z);
    	child.setYPosition(y);

    }

    public int getBaselineAngle(CarouselAffinity a) {
    	if (a == CarouselAffinity.LEFT)
    		return 0;
    	if (a == CarouselAffinity.RIGHT)
    		return 180;
    	throw new UnsupportedOperationException();
    }	


    private void calculate3DPosition(CarouselView child, int radius, float angleOffset, CarouselAffinity a){
    	//Log.d(TAG, "index="+child.getIndex()+",radius="+radius+ ",angleOffset="+angleOffset) ;
		//Log.d(TAG, "affinity="+a) ;
		float k = Math.abs(cosOfHalfAngleSpan * radius);
		//float l = Math.abs(sinOfHalfAngleSpan * radius);
		float l = getHeight()/2;
    	angleOffset = (float) ((getBaselineAngle(mAffinity) - angleOffset) * (Math.PI/180.0f));
    	
    	float m = (float) (radius * Math.cos(angleOffset));
    	float x = m - k;
    	if (mAffinity == CarouselAffinity.RIGHT) {
    		x = getRight() - x;
    	}

    	float y = (float) (l - ((radius) *Math.sin(angleOffset)));
    	float z = 0;
    	
		//Log.d(TAG, "x="+x+",y="+y+",z="+z) ;
		
    	child.setXPosition(x);
    	child.setZPosition(z);
    	child.setYPosition(y);
    }

    private boolean dispatchLongPress(View view, int position, long id) {
        boolean handled = false;
        
        if (mOnItemLongClickListener != null) {
            handled = mOnItemLongClickListener.onItemLongClick(getAdapter(), mDownTouchView,
                    mDownTouchPosition, id);
        }

        if (!handled) {
            mContextMenuInfo = new AdapterContextMenuInfo(view, position, id);
            handled = super.showContextMenuForChild(this);
        }

        if (handled) {
            performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
        }
        
        return handled;
    }    
    
    private void dispatchPress(View child) {
        
        if (child != null) {
            child.setPressed(true);
        }
        
        setPressed(true);
    }
    
    private void dispatchUnpress() {
        
        for (int i = getChildCount() - 1; i >= 0; i--) {
            getChildAt(i).setPressed(false);
        }
        
        setPressed(false);
    }            
	
	
    /**
     * @return The center of the given view.
     */
    private static int getCenterOfView(View view) {
        return view.getLeft() + view.getWidth() / 2;
    }	
		
    

    private void makeAndAddView(int position, float angleOffset) {
        CarouselView child;
  
        if (!mDataChanged) {
            child = (CarouselView)mRecycler.get(position);
            if (child != null) {

                //((TextView)child.findViewById(R.id.cancelButton)).setText(";;"+position+" Item;;");
                // Position the view
                //setupChild(child, child.getIndex(), angleOffset);
                setupChild(child, position, angleOffset);
            }
            else
            {
                // Nothing found in the recycler -- ask the adapter for a view
                child = (CarouselView)mAdapter.getView(position, null, this);
                //((TextView)child.findViewById(R.id.cancelButton)).setText(";;"+position+" Item;;");

                // Position the view
                //setupChild(child, child.getIndex(), angleOffset);            	
                setupChild(child, position, angleOffset);            	
            }
            //child.setIndex(position);
            return;
        }

        // Nothing found in the recycler -- ask the adapter for a view
        child = (CarouselView)mAdapter.getView(position, null, this);
        //((TextView)child.findViewById(R.id.cancelButton)).setText(";;"+position+" Item;;");

        // Position the view
        //setupChild(child, child.getIndex(), angleOffset);
        setupChild(child, position, angleOffset);
        //child.setIndex(position);

    }      
    
    void onCancel(){
    	onUp();
    }


  	private Canvas mCachedCanvas = new Canvas();
    private Bitmap mCachedBitmap;
    private Paint mCachedPaint=new Paint();
    @SuppressLint("WrongCall")
	private void generateCacheBitmap(){
    	if (mCachedBitmap == null ||
    		mCachedBitmap.getHeight() != getHeight() ||
    		mCachedBitmap.getWidth() != getWidth())
    	{
    		mCachedPaint.setColor(Color.WHITE);
    		mCachedPaint.setAlpha(200);
    		mCachedPaint.setStyle(Paint.Style.FILL_AND_STROKE);
    		mCachedPaint.clearShadowLayer();
    		CarouselView cv = (CarouselView)getChildAt(mSelectedPosition);
    		int width=cv.getWidth() * 6/8;
    		//Log.d(TAG, "geneating BitMap");
    		mCachedBitmap = Bitmap.createBitmap(getWidth(), getHeight(),  Bitmap.Config.ARGB_8888);
    		mCachedBitmap.setDensity(getResources().getDisplayMetrics().densityDpi);
    		mCachedCanvas.setBitmap(mCachedBitmap);
    		mCachedPaint.setShadowLayer(23, -3, 2, 0xFF444444);
    		mCachedPaint.setDither(true);
    		mCachedCanvas.drawCircle(mCenter.x-width-getLeft()-getPaddingRight(),
    				mCenter.y, mRadius, mCachedPaint);
    		
    		int childHeight = this.getChildAt(0).getHeight();

    		mCachedPaint.setShadowLayer(3, -3, 2, 0x44444444);
    		lipDrawRect.left  = getWidth() - cv.getWidth() -getPaddingRight() - LIP_WIDTH;
    		lipDrawRect.right = getRight()+10;
    		mCachedPaint.setColor(Color.WHITE);
    		mCachedPaint.setAlpha(200);
    		lipDrawRect.top  = getHeight()/2 - childHeight/2;
    		lipDrawRect.bottom= getHeight()/2 + childHeight/2;
    		mCachedCanvas.drawRoundRect(lipDrawRect, 11, 11, mCachedPaint);
    		mCachedPaint.setColor(Color.BLACK);
    		mCachedPaint.setTextSize(21);
    		mCachedPaint.setTextScaleX(2);
    		mCachedCanvas.drawText(m2.avg+"", 10,110, mCachedPaint);
    	}
    }
    
    private void invalidateCache() {
    	mCachedBitmap = null;
    }

	RectF lipDrawRect = new RectF();
   	Paint p = new Paint();
   	public static final int LIP_WIDTH=100;
    @SuppressLint("DrawAllocation")
	protected void onDraw(Canvas canvas) {
    	//Rect canvasR = new Rect();
    	//canvas.getClipBounds(canvasR);
    	//Matrix m =canvas.getMatrix();
    	//Rect viewR = new Rect();
    	//this.getLocalVisibleRect(viewR);
    	//Log.d(TAG, "canvasR="+canvasR);
    	//Log.d(TAG, "m="+m);
    	//Log.d(TAG, "viewR="+viewR);
        CarouselView cv = (CarouselView)getChildAt(mSelectedPosition);
        int width=cv.getWidth();
        width=width*6/8;
    	p.setColor(Color.WHITE);
    	p.setAlpha(200);
   		p.setStyle(Paint.Style.FILL_AND_STROKE);

    	int childHeight = this.getChildAt(0).getHeight();
    	if (mAffinity == CarouselAffinity.LEFT) {
    		p.clearShadowLayer();
    		canvas.drawCircle(getWidth()+40-mCenter.x, mCenter.y, canvas.getWidth()-70, p);
    		lipDrawRect.left  = -10;
    		lipDrawRect.right = getWidth()-250;
    	}
    	if (mAffinity == CarouselAffinity.RIGHT) {
    		m2.start();
    		//drawing a circle with a shadowlayer is really really slow -- 100 ms
    		generateCacheBitmap();
    		canvas.drawBitmap(mCachedBitmap, 0, 0, null);
    		//p.clearShadowLayer();
    		//canvas.drawCircle(mCenter.x-width-getLeft()-getPaddingRight(), mCenter.y, mRadius, p);
    		m2.end();
    	}

    	/*
    	p.setDither(true);
    	p.setShadowLayer(3, -3, 2, 0x44444444);
    	//p.setColor(Color.GREEN);
    	//canvas.drawLine(0, canvas.getHeight()/2, canvas.getWidth(), canvas.getHeight()/2, p);
    	//p.setColor(Color.BLUE);
    	//canvas.drawLine(0, getHeight()/2, getWidth(), getHeight()/2, p);
    	//canvas.drawRect(viewR, p);


    	p.setColor(Color.WHITE);
    	p.setAlpha(200);
    	lipDrawRect.top  = getHeight()/2 - childHeight/2;
    	lipDrawRect.bottom= getHeight()/2 + childHeight/2;
    	canvas.drawRoundRect(lipDrawRect, 11, 11, p);
    	p.setColor(Color.BLACK);
    	p.setTextSize(21);
    	p.setTextScaleX(2);
    	canvas.drawText(m2.avg+"", 10,110, p);
    	//canvas.drawRect(0, canvas.getHeight()/2-60,canvas.getWidth()-100,  canvas.getHeight()/2+60, p);
    	 *
    	 */

    	super.onDraw(canvas);
    }
    /**
     * Called when rotation is finished
     */
    private void onFinishedMovement() {
        if (mSuppressSelectionChanged) {
            mSuppressSelectionChanged = false;
            
            // We haven't been callbacking during the fling, so do it now
            selectionChanged1();
        }
        invalidate();
    }    
         
    void onUp(){
    	//Log.d(TAG,"onUp");
        if (mFlingRunnable.mRotator.isFinished()) {
            scrollIntoSlots();
        }        
        dispatchUnpress();    	
    }
         
    /**
     * Brings an item with nearest to 0 degrees angle to this angle and sets it selected 
     */
    private void scrollIntoSlots(){
    	//Log.d(TAG, "scrollIntoSlots(" + ") mSelectedChild="+mSelectedChild);
    	// Nothing to do
        if (getChildCount() == 0 || mSelectedChild == null) return;
        
    	float angleFirst, angleLast, angleToRotate;
    	int position;
        angleFirst = ((CarouselView)getAdapter().getView(0, null, null)).getCurrentAngle();
        angleLast = ((CarouselView)getAdapter().getView(getCount()-1, null, null)).getCurrentAngle();
        angleFirst = angleFirst - getBaselineAngle(mAffinity);
        angleLast = angleLast - getBaselineAngle(mAffinity);

    	//prevent a loop by short-circuiting angles that are really really small

        Log.w(TAG, "scrollIntoSlots smallest angle="+angleFirst);
        Log.w(TAG, "scrollIntoSlots largest angle="+angleLast);

   		int count = getItemsToShowCount();
   		float angleUnit = (mEndAngle - mStartAngle) / count;

   		if (angleFirst<0 && angleLast>0) {
   			angleToRotate = angleFirst % angleUnit;

   			// Make it minimum to rotate
   			if (angleToRotate > angleUnit/2)
   				angleToRotate = angleToRotate- angleUnit;
   		} else {
   			angleToRotate = angleFirst;

   			if (angleLast<0)
   				angleToRotate = angleLast;
   		}

 
   		
    	if (Math.abs(angleToRotate) < 0.01)
    		angleToRotate = 0;
        Log.w(TAG, "-scrollIntoSlots angle="+angleToRotate);
        // Start rotation if needed
        if(angleToRotate != 0.0f) {
        	mFlingRunnable.startUsingDistance(-angleToRotate);
        }
        else {
        	ArrayList<CarouselView> arr = new ArrayList<CarouselView>();
        	for(int i = 0; i < getAdapter().getCount(); i++) 
        		arr.add(((CarouselView)getAdapter().getView(i, null, null)));
        	Collections.sort(arr, new Comparator<CarouselView>(){
        		public int compare(CarouselView c1, CarouselView c2) {
        			int a1 = (int)(c1.getCurrentAngle() - getBaselineAngle(mAffinity));
        			int a2 = (int)(c2.getCurrentAngle() - getBaselineAngle(mAffinity));

        			return (Math.abs(a1) - Math.abs(a2)) ;
        		}
        	});
 
            // Set selected position
            position = arr.get(0).getIndex();
            setSelectedPositionInt(position);
        	onFinishedMovement();
        }
    }
       
    /**
     * Brings an item with nearest to 0 degrees angle to this angle and sets it selected 
     */
    private void scrollIntoSlots_old(){
    	//Log.d(TAG, "scrollIntoSlots(" + ") mSelectedChild="+mSelectedChild);

    	// Nothing to do
        if (getChildCount() == 0 || mSelectedChild == null) return;
        
        // get nearest item to the 0 degrees angle
        // Sort itmes and get nearest angle
    	float smallestAngle=-360; 
    	float largestAngle=360; 
    	int position;
    	
    	/*
    	int tempAngle;
        for(int i = 0; i < getAdapter().getCount(); i++)  {
        	tempAngle = (int) Math.round(((CarouselView)getAdapter().getView(i, null, null)).getCurrentAngle());
        	if (tempAngle < smallestAngle) {
        		smallestAngle = tempAngle;
        	}
        }
        */
    	ArrayList<CarouselView> arr = new ArrayList<CarouselView>();
        for(int i = 0; i < getAdapter().getCount(); i++) 
        	arr.add(((CarouselView)getAdapter().getView(i, null, null)));
        Collections.sort(arr, new Comparator<CarouselView>(){
			public int compare(CarouselView c1, CarouselView c2) {
				int a1 = (int)((c1.getCurrentAngle() + getBaselineAngle(mAffinity)) % 360.0);
				int a2 = (int)((c2.getCurrentAngle() + getBaselineAngle(mAffinity)) % 360.0);
				//if(a1 > 180)
					//a1 = 360 - a1;
				//if(a2 > 180)
					//a2 = 360 - a2;

				return (a1 - a2) ;
			}
        });
        smallestAngle = (arr.get(0).getCurrentAngle()- getBaselineAngle(mAffinity)) % 360;
        largestAngle = (arr.get(arr.size()-1).getCurrentAngle()- getBaselineAngle(mAffinity)) % 360;

    	//prevent a loop by short-circuiting angles that are really really small
    	if (Math.abs(smallestAngle) < 0.01)
    		smallestAngle = 0;

    	if (Math.abs(largestAngle) < 0.01)
    		largestAngle = 0;

        Log.w(TAG, "scrollIntoSlots smallest angle="+smallestAngle);
        Log.w(TAG, "scrollIntoSlots largest angle="+largestAngle);

   		int count = getItemsToShowCount();
   		float angleUnit = (mEndAngle - mStartAngle) / count;
   		smallestAngle = smallestAngle % angleUnit;

   		// Make it minimum to rotate
   		if (smallestAngle > angleUnit/2)
   			smallestAngle = smallestAngle- angleUnit;
   		
   		
    	//if(angle > 180.0f)
    		//angle = -(360.0f - angle);

        //Log.w(TAG, "-scrollIntoSlots angle="+angle);
        // Start rotation if needed
        if(smallestAngle != 0.0f)
        {
        	mFlingRunnable.startUsingDistance(-smallestAngle);
        }
        else
        {
            // Set selected position
            position = arr.get(0).getIndex();
            setSelectedPositionInt(position);
        	onFinishedMovement();
        }
    }
    
	void scrollToChild(int i){		
		CarouselView view = (CarouselView)getAdapter().getView(i, null, null);
		float angle = view.getCurrentAngle();
		//Log.d(TAG, "scrollToChild:index="+i+",angle="+angle);
		angle = getBaselineAngle(mAffinity) - angle;		
		if(angle == 0)
			return;
		
		if(angle > 180.0f)
			angle = 360.0f - angle;
		else
			angle = -angle;

    	mFlingRunnable.startUsingDistance(-angle);

		
	}    
    
    /**
     * Whether or not to callback on any {@link #getOnItemSelectedListener()}
     * while the items are being flinged. If false, only the final selected item
     * will cause the callback. If true, all items between the first and the
     * final will cause callbacks.
     * 
     * @param shouldCallback Whether or not to callback on the listener while
     *            the items are being flinged.
     */
    public void setCallbackDuringFling(boolean shouldCallback) {
        mShouldCallbackDuringFling = shouldCallback;
    }
    
    /**
     * Whether or not to callback when an item that is not selected is clicked.
     * If false, the item will become selected (and re-centered). If true, the
     * {@link #getOnItemClickListener()} will get the callback.
     * 
     * @param shouldCallback Whether or not to callback on the listener when a
     *            item that is not selected is clicked.
     * @hide
     */
    public void setCallbackOnUnselectedItemClick(boolean shouldCallback) {
        mShouldCallbackOnUnselectedItemClick = shouldCallback;
    }
    
    /**
     * Sets how long the transition animation should run when a child view
     * changes position. Only relevant if animation is turned on.
     * 
     * @param animationDurationMillis The duration of the transition, in
     *        milliseconds.
     * 
     * @attr ref android.R.styleable#Gallery_animationDuration
     */
    public void setAnimationDuration(int animationDurationMillis) {
        mAnimationDuration = animationDurationMillis;
    }
            

    private static final float radianPerDegree = (float) (Math.PI/180.0f);

    /**
     * Helper function to calculate the diameter of the arc.
     * The arc is defined by the startAngle and the endAngle and the height of the chord.
     * 
     * @param startAngle The angle at which the arc starts in degrees
     * @param endAngle The angle at which the arc ends in degrees
     * @param chordLength The length of the chord  in pixels
     */
	public int getRadius() {
		return mRadius;
	}
	public static int findRadius(float angleSpan, int chordLength) {
    	//Log.i(TAG, "angleSpan="+angleSpan+",chord length = "+ chordLength);
    	float radius =  (float) Math.sin(angleSpan*radianPerDegree/2);
    	radius =  Math.abs((chordLength/2)/radius);
    	//Log.d(TAG, "calculated radius = "+ radius);
    	return (int)radius;
	}

    /**
     * Helper for makeAndAddView to set the position of a view and fill out its
     * layout paramters.
     * 
     * @param child The view to position
     * @param offset Offset from the selected position
     * @param x X-coordintate indicating where this view should be placed. This
     *        will either be the left or right edge of the view, depending on
     *        the fromLeft paramter
     * @param fromLeft Are we posiitoning views based on the left edge? (i.e.,
     *        building from left to right)?
     */
    private void setupChild(CarouselView child, int index, float angleOffset) {
    	// Ignore any layout parameters for child, use wrap content
        addViewInLayout(child, -1 /*index*/, generateDefaultLayoutParams());

        child.setSelected(index == mSelectedPosition);

        int h, parentH;
        int w, parentW;

        if(mInLayout) {
        	//Log.d(TAG, "mInLayout=true");
	        h = child.getMeasuredHeight();// - getPaddingBottom() - getPaddingTop();
	        w = child.getMeasuredWidth();// - getPaddingLeft() - getPaddingRight(); 
	        parentH = getMeasuredHeight();// - getPaddingBottom() - getPaddingTop();
	        parentW = getMeasuredWidth();// - getPaddingLeft() - getPaddingRight(); 
        }
        else {
        	//Log.d(TAG, "mInLayout=false");
	        h = child.getHeight();
	        w = child.getWidth();        	
	        parentH = getHeight();
	        parentW = getWidth();        	
        }
		//Log.d(TAG, "h="+h+ ",w="+ w+ ",parentH="+parentH+ ",parentW="+parentW);

        child.setCurrentAngle(angleOffset);

        //int wMeasureSpec = getChildMeasureSpec (View.MeasureSpec.UNSPECIFIED,0,w);
        //int hMeasureSpec = getChildMeasureSpec (View.MeasureSpec.UNSPECIFIED,0,h);
        // Measure child
        //child.measure(wMeasureSpec, hMeasureSpec);

        //measureChild(child, w, h);
        //child.measure(w, h);

        //int childLeft=0, childTop = 0;
        calculate3DPosition(child, mRadius, angleOffset, mAffinity);
		//Log.d(TAG, "index="+index+",Left="+childLeft+",Top="+childTop+",right="+w+",bottom="+h) ;

		int x = (int) child.getXPosition() - w/2;
		int y = (int) child.getYPosition() - h/2;
		float k = Math.abs(cosOfHalfAngleSpan * mRadius);
		int extraShift = (int) (w/2-(mRadius-k));

		child.layout(x-extraShift-getLeft()-getPaddingRight(), y, w+x-extraShift-getLeft()-getPaddingRight(), h+y);
		//Log.d(TAG, "index="+index+
		           //",getLeft="+child.getLeft()+
				   //",getRight="+child.getRight()+
				   //",getTop="+child.getTop()+
				   //",getBottom="+child.getBottom()) ;
    } 

    class Measurement {
    	public long avg=0;
    	long start,end;
    	public void start() {
    		start = Calendar.getInstance().getTimeInMillis();
    	}
    	public void end() {
    		end = Calendar.getInstance().getTimeInMillis();
    		avg = avg + (end - start);
    		avg = avg/2;
    	}
    	public long diff() {
    		return end-start;
    	}
    }
    	
	
    Measurement m1 = new Measurement();
    Measurement m2 = new Measurement();
    Measurement m3 = new Measurement();
    /**
     * Tracks a motion scroll. In reality, this is used to do just about any
     * movement to items (touch scroll, arrow-key scroll, set an item as selected).
     * 
     * @param deltaAngle Change in X from the previous event.
     */
    void trackMotionScroll(float deltaAngle) {
    	m1.start();
    	if (deltaAngle==0)
    		return;
    	
    	//Log.w(TAG,"trackMotionScroll(deltaAngle="+deltaAngle+")");
        if (getChildCount() == 0) {
            return;
        }

        int w, h;
        for(int i = 0; i < getAdapter().getCount(); i++){
        	CarouselView child = (CarouselView)getAdapter().getView(i, null, null);
        	float angle = child.getCurrentAngle();
            int y = (int) child.getYPosition();
        	//Log.w(TAG,"i="+i+",angle="+angle+",y="+y);
        	angle += deltaAngle;
        	while(angle > 360.0f)
        		angle -= 360.0f;
        	while(angle < 0.0f)
        		angle += 360.0f;
        	child.setCurrentAngle(angle);
            calculate3DPosition(child, mRadius, angle, mAffinity);        	
            y = (int) child.getYPosition();
        	//Log.w(TAG,"i="+i+",angle="+angle+",y="+y);
            w = child.getWidth();
            h = child.getHeight();
            int x = (int) child.getXPosition() - w/2;
            y =  y- h/2;

            float k = Math.abs(cosOfHalfAngleSpan * mRadius);
            int extraShift = (int) (w/2-(mRadius-k));
            child.layout(x-extraShift-getLeft()-getPaddingRight(), y, w+x-extraShift-getLeft()-getPaddingRight(), h+y);
        }

        // Clear unused views
        mRecycler.clear();        
        
        invalidate();
        m1.end();
    }	
	
    private void updateSelectedItemMetadata() {

        View oldSelectedChild = mSelectedChild;

        View child = mSelectedChild = getChildAt(mSelectedPosition - mFirstPosition);
        if (child == null) {
            return;
        }

        child.setSelected(true);
        child.setFocusable(true);

        if (hasFocus()) {
            child.requestFocus();
        }

        // We unfocus the old child down here so the above hasFocus check
        // returns true
        if (oldSelectedChild != null) {

            // Make sure its drawable state doesn't contain 'selected'
            oldSelectedChild.setSelected(false);

            // Make sure it is not focusable anymore, since otherwise arrow keys
            // can make this one be focused
            oldSelectedChild.setFocusable(false);
        }
    }	
}
