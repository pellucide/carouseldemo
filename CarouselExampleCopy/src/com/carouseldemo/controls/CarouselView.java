package com.carouseldemo.controls;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.MeasureSpec;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class CarouselView extends RelativeLayout implements Comparable<CarouselView> {

	@Override
	public boolean dispatchTouchEvent(MotionEvent ev) {
        //Log.d(TAG, this+":dispatchTouchEvent");
        boolean ret ;
		ret = super.dispatchTouchEvent(ev);
        //Log.d(TAG, "returning ="+ret);
		return ret;
	}


	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev) {
        //Log.d(TAG, this+":onInterceptTouchEvent");
        boolean ret ;
		ret = super.onInterceptTouchEvent(ev);
        //Log.d(TAG, "returning ="+ret);
		return ret;
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
        //Log.d(TAG, this+":onTouchEvent");
        boolean ret ;
		ret = super.onTouchEvent(ev);
        //Log.d(TAG, "returning ="+ret);
		return ret;
	}

	public static final String TAG = CarouselView.class.getSimpleName();
	private int index;
	private float currentAngle;
	private float x;
	private float y;
	private float z;
	private boolean drawn;
	//private RelativeLayout layout;
	
	public CarouselView(Context context) {//, int layoutResourceId) {
		this(context, null, 0);//, layoutResourceId);
	}	

	public CarouselView(Context context, AttributeSet attrs) {//, int layoutResourceId) {
		this(context, attrs, 0);//, layoutResourceId);
	}
	
	public CarouselView(Context context, AttributeSet attrs, int defStyle) {//, int layoutResourceId) {
		super(context, attrs, defStyle);
		//LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		//layout = (RelativeLayout) inflator.inflate(layoutResourceId, null);
	}


	public void setIndex(int index) {
		this.index = index;
	}
	
	//public RelativeLayout getLayout() {
		//return layout;
	//}

	public int getIndex() {
		return index;
	}

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //Log.d(TAG, "widthMeasureSpec="+View.MeasureSpec.toString(widthMeasureSpec));
        //Log.d(TAG, "heightMeasureSpec="+View.MeasureSpec.toString(heightMeasureSpec));
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
    	//Log.d(TAG, "onLayout(changed:"+changed+" l:"+l+" t:"+t+" r:"+r+" b:"+b+")");
        super.onLayout(changed, l, t, r, b) ;
    }

	public void setCurrentAngle(float currentAngle) {
		this.currentAngle = currentAngle;
	}

	public float getCurrentAngle() {
		return currentAngle;
	}

	public int compareTo(CarouselView another) {
		return (int)(another.z - this.z);
	}

	public void setXPosition(float x) {
		this.x = x;
	}

	public float getXPosition() {
		return x;
	}

	public void setYPosition(float y) {
		this.y = y;
	}

	public float getYPosition() {
		return y;
	}

	public void setZPosition(float z) {
		this.z = z;
	}

	public float getZPosition() {
		return z;
	}

	public void setDrawn(boolean drawn) {
		this.drawn = drawn;
	}

	public boolean isDrawn() {
		return drawn;
	}
}
